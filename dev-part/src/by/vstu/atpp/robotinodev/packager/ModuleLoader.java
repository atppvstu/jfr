package by.vstu.atpp.robotinodev.packager;

import java.io.IOException;
import java.io.InputStream;

import by.vstu.atpp.robotino.module.ModulePackage;

/**
 * This class is used to save Modules and Actions as {@link ModulePackage
 * ModulePackage}.
 * 
 * @author isap.vstu.by
 */
public class ModuleLoader {

	/**
	 * Loads class and pack it to ModulePackage.
	 * 
	 * @param clazz
	 *            The class to pack.
	 * @return ModulePackage with class byte code and it's name.
	 * @throws IOException
	 *             If class file is not accesible.
	 */
	public ModulePackage load(Class<?> clazz) throws IOException {
		String className = clazz.getName();
		String classAsPath = className.replace('.', '/') + ".class";
		try (InputStream stream = clazz.getClassLoader().getResourceAsStream(classAsPath)) {
			int length = stream.available();
			byte[] array = new byte[length];
			stream.read(array);
			return new ModulePackage(clazz.getName(), array);
		}

	}
}
