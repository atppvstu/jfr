package by.vstu.atpp.robotinodev.packager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import by.vstu.atpp.robotino.module.ModulePackage;

/**
 * This class save ModulePackage as a file on specified path.
 * 
 * @author isap.vstu.by
 *
 */
public class ModulePacker {

	/**
	 * File with Module extension
	 */
	public static final String MODULE_EXTENSION = ".r3m";

	/**
	 * File with Action extension
	 */
	public static final String ACTION_EXTENSION = ".r3a";

	/**
	 * Save ModulePackage as a file.
	 * 
	 * @param pack
	 *            ModulePackage to save.
	 * @param path
	 *            Path to save.
	 * @param extension
	 *            extension of the file.
	 * @throws IOException
	 *             if cannot create file.
	 */
	public void pack(ModulePackage pack, String path, String extension) throws IOException {
		String name = pack.getModuleClass();
		name = name.substring(name.lastIndexOf('.') + 1);
		String filename = path + File.separator + name + extension;
		try (ObjectOutputStream stream = new ObjectOutputStream(new FileOutputStream(new File(filename)))) {
			stream.writeObject(pack);
			stream.flush();
		}

	}
}
