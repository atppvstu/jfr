package by.vstu.atpp.robotinodev.runner;

import static by.vstu.atpp.robotinodev.packager.ModulePacker.ACTION_EXTENSION;
import static by.vstu.atpp.robotinodev.packager.ModulePacker.MODULE_EXTENSION;
import static java.util.jar.Attributes.Name.MANIFEST_VERSION;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;

import com.google.common.io.Files;

import by.vstu.atpp.robotino.action.Action;
import by.vstu.atpp.robotino.module.Module;
import by.vstu.atpp.robotino.module.ModulePackage;
import by.vstu.atpp.robotinodev.packager.ModuleLoader;
import by.vstu.atpp.robotinodev.packager.ModulePacker;

/**
 * This class is used for export custom modules into the
 * JavaFrameworkForRobotino.
 *
 * @author isap.vstu.by
 *
 */
public class Packager {

    /**
     * Save Modules and Action as a files into specified directory.
     *
     * For example: *
     *
     * <pre>
     * <code>
     * String path = "C:/MyDir/"
     *
     * Packager.packageClasses(new Class&lt;?&gt;[] { TestModule1.class,
     * TestModule2.class, TestAction.class }, path);
     * </code>
     * </pre>
     *
     * @param path
     *            Folder to save.
     * @param classes
     *            Classes to export.
     */
    public static void packageClasses(String path, Class<?>... classes) {
        System.out.println("packaging classes:");
        File dir = new File(path);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        ModulePacker packer = new ModulePacker();
        ModuleLoader loader = new ModuleLoader();
        ModulePackage pack;
        try {
            for (Class<?> clazz : classes) {
                System.out.println("\t" + clazz.getName());
                List<Class<?>> interfaces = new ArrayList<>();
                interfaces.addAll(Arrays.asList(clazz.getInterfaces()));
                interfaces.addAll(Arrays.asList(clazz.getSuperclass().getInterfaces()));
                for (Class<?> intrf : interfaces) {
                    String extension;
                    if (intrf.equals(Module.class)) {
                        extension = MODULE_EXTENSION;
                    } else if (intrf.equals(Action.class)) {
                        extension = ACTION_EXTENSION;
                    } else {
                        continue;
                    }
                    pack = loader.load(clazz);
                    packer.pack(pack, path, extension);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("done.");
    }

    /**
     * Save Action and Modules as a JAR file to use in another custom Actions and
     * Modules.
     *
     * For example:
     *
     * <pre>
     * <code>
     * Packager.packageJar("C://modules", "myJar.jar", true, WallFollowModule.class, MyAction.class);
     * </code>
     * </pre>
     *
     * @param path
     *            Folder to save.
     * @param name
     *            Name of the file which will be created.
     * @param copyLibraries
     *            Libraries from the project will be copied into the JAR if true.
     * @param classes
     *            Classes to save in JAR.
     */
    public static void packageJar(String path, String name, boolean copyLibraries, Class<?>... classes) {
        System.out.println("archiving classes:");
        File dir = new File(path);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        if (!name.endsWith(".jar")) {
            name += ".jar";
        }
        if (!path.endsWith("/")) {
            path += "/";
        }
        JarEntry entry;
        ModulePackage pack;
        ModuleLoader loader = new ModuleLoader();
        Manifest manifest = new Manifest();
        manifest.getMainAttributes().put(MANIFEST_VERSION, "1.0");
        try (JarOutputStream stream = new JarOutputStream(new FileOutputStream(new File(path + name)), manifest)) {
            for (Class<?> clazz : classes) {
                System.out.println("\t" + clazz.getName());
                entry = new JarEntry(clazz.getName().replaceAll("\\.", "/") + ".class");
                entry.setTime(System.currentTimeMillis());
                stream.putNextEntry(entry);
                pack = loader.load(clazz);
                stream.write(pack.getClassByteArray());
                stream.closeEntry();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("done.");
        if (copyLibraries) {
            copyLibraries(path);
        }
    }

    private static void copyLibraries(String path) {
        System.out.println("copying libraries:");
        File dir = new File(path);
        if (!path.endsWith("/")) {
            path += "/";
        }
        if (!dir.exists()) {
            dir.mkdirs();
        }
        String[] libs = System.getProperty("java.class.path").split(";");
        File fromFile;
        File toFile;
        try {
            for (String lib : libs) {
                if (lib.endsWith(".jar")) {
                    String libName = lib.substring(lib.lastIndexOf(File.separatorChar) + 1, lib.length());
                    System.out.println("\t" + libName);
                    fromFile = new File(lib);
                    toFile = new File(path + libName);
                    Files.copy(fromFile, toFile);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("done.");
    }
}
