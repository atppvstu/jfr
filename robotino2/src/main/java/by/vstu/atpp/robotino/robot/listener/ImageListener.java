package by.vstu.atpp.robotino.robot.listener;

import java.awt.Image;

public interface ImageListener {

    /**
     * Invokes when image will be received from the robot.
     * 
     * @param img
     *            received image
     */
    void onImageReceived(Image img);

}
