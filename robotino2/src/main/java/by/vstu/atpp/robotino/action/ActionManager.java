package by.vstu.atpp.robotino.action;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.annotation.PostConstruct;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.google.common.util.concurrent.ListenableScheduledFuture;

import by.vstu.atpp.robotino.loader.RefreshableApplicationContextHolder;

/**
 * ActionManager manages the execution actions. This class can start an action
 * by it's name and cansel current action.
 *
 * @author isap.vstu.by
 *
 */
@Component
public class ActionManager {

    private static final Log LOG = LogFactory.getLog(ActionManager.class);

    @Autowired
    private RefreshableApplicationContextHolder ctxHolder;

    private Action current;
    private ExecutorService service = Executors.newSingleThreadExecutor();
    private List<String> actionNames = new ArrayList<>();

    @PostConstruct
    private void init() {
	refreshActions();
    }

    private void refreshActions() {
	ApplicationContext ctx = ctxHolder.getContext();
	actionNames.clear();
	actionNames.addAll(Arrays.asList(ctx.getParent().getBeanNamesForType(Action.class)));
	actionNames.addAll(Arrays.asList(ctx.getBeanNamesForType(Action.class)));
    }

    /**
     * This method starts an action according with his name.
     *
     * @param id
     *            Name of the action that will be executed.
     * @param callback
     *            invoked after action ends.
     * @return true if action was started
     */
    public boolean execute(String id, Runnable callback) {
	current = getInstance(id);
	if (current == null) {
	    return false;
	}
	ListenableScheduledFuture<?> lsf = current.execute();
	lsf.addListener(callback, service);
	return true;
    }

    private Action getInstance(String id) {
	Action action = null;
	try {
	    action = ctxHolder.getContext().getBean(id, Action.class);
	} catch (BeansException e) {
	    LOG.info(e.getMessage());
	}
	return action;
    }

    /**
     * Cancel current action
     */
    public void cancel() {
	if (current != null) {
	    current.cancel();
	    current = null;
	}
    }

    /**
     * Get names of available actions
     *
     * @return List with names of actions
     */
    public List<String> getActionNames() {
	refreshActions();
	return actionNames;
    }

}
