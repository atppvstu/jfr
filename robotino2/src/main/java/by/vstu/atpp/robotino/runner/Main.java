package by.vstu.atpp.robotino.runner;

import by.vstu.atpp.robotino.util.JarLoader;
import by.vstu.atpp.robotino.view.MainFrame;

/**
 * This is main class of framework runs the application.
 * 
 * @author isap.vstu.by
 *
 */
public class Main {

    /**
     * Main method.
     * 
     * @param args
     *            Run parameters
     */
    public static void main(String[] args) {
	JarLoader.updateLibraries();
	MainFrame.launch(MainFrame.class, args);
    }
}
