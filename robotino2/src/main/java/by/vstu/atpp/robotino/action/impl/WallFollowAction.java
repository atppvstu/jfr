package by.vstu.atpp.robotino.action.impl;

import by.vstu.atpp.robotino.action.AbstractAction;
import by.vstu.atpp.robotino.action.ActionClass;
import by.vstu.atpp.robotino.module.impl.WallFollowModule;

/**
 * This action consists of the following modules:
 * <ul>
 * <li>{@link WallFollowModule WallFollowModule}</li>
 * </ul>
 * 
 * @author isap.vstu.by
 *
 */
@ActionClass
public class WallFollowAction extends AbstractAction {

    /**
     * 
     * @param module
     *            The module to perform.
     */
    public WallFollowAction(WallFollowModule module) {
	addLast(module);
    }

}
