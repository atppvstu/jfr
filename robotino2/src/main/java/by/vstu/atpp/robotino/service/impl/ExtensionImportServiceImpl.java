package by.vstu.atpp.robotino.service.impl;

import java.io.File;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import by.vstu.atpp.robotino.event.ActionsRefreshedEvent;
import by.vstu.atpp.robotino.loader.ExtensionBeanDefinitionRegistryPostProcessor;
import by.vstu.atpp.robotino.loader.ExtensionManager;
import by.vstu.atpp.robotino.loader.RefreshableApplicationContextHolder;
import by.vstu.atpp.robotino.service.ExtensionImportService;
import by.vstu.atpp.robotino.util.PathIOUtil;

/**
 * This class is default implementation of the {@link ExtensionImportService
 * ExtensionImportService}.
 *
 * @author isap.vstu.by
 *
 */
@Service
public class ExtensionImportServiceImpl implements ExtensionImportService {

    @Autowired
    private RefreshableApplicationContextHolder ctxHolder;
    @Autowired
    private BeanFactory factory;

    @PostConstruct
    private void init() {
	importFrom(PathIOUtil.loadFilesAsList());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void importFrom(List<File> folders) {
	ExtensionManager manager = new ExtensionManager(folders);
	ctxHolder.getContext().addBeanFactoryPostProcessor(
		new ExtensionBeanDefinitionRegistryPostProcessor(factory, manager.getClasses()));
	ctxHolder.refresh();
	publishEvent();
    }

    private void publishEvent() {
	ctxHolder.getContext().getParent().publishEvent(new ActionsRefreshedEvent());
    }
}
