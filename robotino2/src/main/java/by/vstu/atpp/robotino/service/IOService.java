package by.vstu.atpp.robotino.service;

import java.util.List;

import rec.robotino.api2.AnalogInput;
import rec.robotino.api2.DigitalInput;
import rec.robotino.api2.DigitalOutput;
import rec.robotino.api2.Relay;

/**
 * This class is used to control IO panel of the robot.
 *
 * @author isap.vstu.by
 *
 */
public interface IOService extends Service {

    /**
     * @return List of DigitalInput (DI1 - DI8)
     */
    List<DigitalInput> getDigitalInputs();

    /**
     * 
     * @return List of AnalogInput (AI1 - AI8)
     */
    List<AnalogInput> getAnalogInputs();

    /**
     * @return List of {@link DigitalOutput DigitalOutputs}.
     */
    List<DigitalOutput> getDigitalOutputs();

    /**
     * @return List of {@link Relay relays}.
     */
    List<Relay> getRalays();
}
