package by.vstu.atpp.robotino.module.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import by.vstu.atpp.robotino.domain.Position;
import by.vstu.atpp.robotino.domain.Speed;
import by.vstu.atpp.robotino.module.AbstractModule;
import by.vstu.atpp.robotino.service.DriveService;
import georegression.metric.UtilAngle;

/**
 * The module turns the robot to the specified angle.
 * 
 * @author isap.vstu.by
 *
 */
@Component
@Scope(scopeName = "prototype")
public class TurnToAngleModule extends AbstractModule {

    @Autowired
    private DriveService driveService;

    private float phiTarget;

    private Position position;
    private float phi;
    private float omega;

    /**
     * {@inheritDoc}
     */
    @Override
    public void perform() {
	position = driveService.getCurrentPosition();
	byte direction = (byte) Math.signum(phiTarget - position.getPhi());
	phi = (float) UtilAngle.dist(position.getPhi(), phiTarget);
	if (Math.abs(phi) < 0.01f) {
	    stop();
	} else {
	    omega = getAngleSpeed(phi * direction);
	}
	driveService.setSpeed(new Speed(0, 0, omega));
    }

    private float getAngleSpeed(float speed) {
	if (Math.abs(speed) > 0.2f) {
	    return 0.2f * Math.signum(speed);
	} else if (Math.abs(speed) < 0.01f) {
	    return 0.01f * Math.signum(speed);
	}
	return speed;
    }

    /**
     * @param phiTarget
     *            angle to turn.
     */
    public void setPhiTarget(float phiTarget) {
	this.phiTarget = phiTarget;
    }

}
