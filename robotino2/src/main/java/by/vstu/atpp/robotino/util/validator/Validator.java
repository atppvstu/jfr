package by.vstu.atpp.robotino.util.validator;

public interface Validator<T> {

    /**
     * Validates entity
     * 
     * @param entity
     *            to validate
     * @return valid entity
     */
    T validate(T entity);

}
