package by.vstu.atpp.robotino.controller.panel;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import by.vstu.atpp.robotino.action.ActionManager;
import by.vstu.atpp.robotino.controller.AbstractController;
import by.vstu.atpp.robotino.event.ActionsRefreshedEvent;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;

/**
 * Controller that handles events of the ModuleWidget.fxml
 * 
 * @author isap.vstu.by
 *
 */
@Component
public class ModuleWidgetController extends AbstractController {

    @FXML
    private ListView<String> list;
    @FXML
    private Button startButton;

    @Autowired
    private ActionManager actionExecutor;
    @Autowired
    private MessageSource messageSource;

    private List<String> ids;

    @FXML
    private void initialize() {
	updateActionNames();
    }

    private void updateActionNames() {
	ids = actionExecutor.getActionNames();
	List<String> names = ids.stream().map(id -> {
	    String name;
	    try {
		name = messageSource.getMessage(id + ".name", null, new Locale("ru", "RU"));
	    } catch (NoSuchMessageException e) {
		name = id;
	    }
	    try {
		name += "\r\t" + messageSource.getMessage(id + ".description", null, new Locale("ru", "RU"));
	    } catch (NoSuchMessageException e) {
	    }
	    return name;
	}).collect(Collectors.toList());

	ObservableList<String> items = FXCollections.observableArrayList(names);

	list.setItems(items);
    }

    @FXML
    private void startAction(ActionEvent event) {
	int index = list.getSelectionModel().getSelectedIndex();
	if (index == -1) {
	    list.setDisable(false);
	} else {
	    startAction(ids.get(list.getSelectionModel().getSelectedIndex()));
	}
    }

    private void startAction(String id) {
	boolean started = actionExecutor.execute(id, this::stopAction);
	if (started) {
	    startButton.setDisable(true);
	    list.setDisable(true);
	}
    }

    @FXML
    private void stopAction() {
	list.setDisable(false);
	actionExecutor.cancel();
	startButton.setDisable(false);
    }

    @EventListener
    private void update(ActionsRefreshedEvent event) {
	updateActionNames();
    }

}
