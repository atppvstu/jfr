package by.vstu.atpp.robotino.util;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;

import javafx.embed.swing.SwingFXUtils;

/**
 * This class is used for convert images between {@link BufferedImage
 * BufferedImage}, {@link Image awt Image} and {@link javafx.scene.image.Image
 * javafx Image}.
 * 
 * @author isap.vstu.by
 *
 */
public final class ImageConverter {

    private ImageConverter() {

    }

    /**
     * Converts a {@link Image awt Image} to the {@link BufferedImage
     * BufferedImage}.
     * 
     * @param imageAwt
     *            image to convert.
     * @return converted image.
     */
    public static BufferedImage toBuffered(Image imageAwt) {
	if (imageAwt instanceof BufferedImage) {
	    return (BufferedImage) imageAwt;
	}
	BufferedImage bufferedImage = new BufferedImage(imageAwt.getWidth(null), imageAwt.getHeight(null),
		BufferedImage.TYPE_INT_RGB);
	Graphics g = bufferedImage.createGraphics();
	g.drawImage(imageAwt, 0, 0, null);
	g.dispose();
	return bufferedImage;
    }

    /**
     * Converts a {@link javafx.scene.image.Image javafx Image} to the
     * {@link BufferedImage BufferedImage}
     * 
     * @param imageFx
     *            image to convert.
     * @return converted image.
     */
    public static BufferedImage toBuffered(javafx.scene.image.Image imageFx) {
	return SwingFXUtils.fromFXImage(imageFx, null);
    }

    /**
     * Converts a {@link BufferedImage BufferedImage} to
     * the{@link javafx.scene.image.Image javafx Image}
     * 
     * 
     * @param bufferedImage
     *            image to convert.
     * @return converted image.
     */
    public static javafx.scene.image.Image toJavaFX(BufferedImage bufferedImage) {
	return SwingFXUtils.toFXImage(bufferedImage, null);
    }

}
