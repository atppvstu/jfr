package by.vstu.atpp.robotino.module.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import by.vstu.atpp.robotino.domain.Speed;
import by.vstu.atpp.robotino.module.AbstractModule;
import by.vstu.atpp.robotino.service.DriveService;

/**
 * The module moves the robot straight.
 * 
 * @author isap.vstu.by
 *
 */
@Component
@Scope(scopeName = "prototype")
public class ForwardModule extends AbstractModule {

    @Autowired
    private DriveService driveService;

    /**
     * {@inheritDoc}
     */
    @Override
    public void perform() {
	driveService.setSpeed(new Speed(0.1f, 0f, 0f));
    }

}
