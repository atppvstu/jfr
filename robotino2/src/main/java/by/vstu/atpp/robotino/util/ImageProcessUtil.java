package by.vstu.atpp.robotino.util;

import static java.lang.Math.max;
import static java.lang.Math.min;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.List;

import boofcv.alg.color.ColorHsv;
import boofcv.alg.filter.binary.BinaryImageOps;
import boofcv.alg.filter.binary.Contour;
import boofcv.io.image.ConvertBufferedImage;
import boofcv.struct.ConnectRule;
import boofcv.struct.image.GrayF32;
import boofcv.struct.image.GrayS32;
import boofcv.struct.image.GrayU8;
import boofcv.struct.image.Planar;
import georegression.metric.UtilAngle;
import georegression.struct.point.Point2D_I32;

/**
 * This class is used for processing image
 *
 * @author isap.vstu.by
 *
 */
public class ImageProcessUtil {

    /**
     * Select a list of contours of an object of a certain color
     *
     * @param image
     *            image to process
     * @param color
     *            target color
     * @param maxDist
     *            max distance from target color
     * @return List of contours
     */
    public static List<Contour> selectAreasNearColor(BufferedImage image, Color color, double maxDist) {

	Planar<GrayF32> input = ConvertBufferedImage.convertFromMulti(image, null, true, GrayF32.class);
	Planar<GrayF32> hsvImage = input.createSameShape();

	float dh, dv;
	float wds, wdh, wdv;
	double curDist;

	float[] hsvColor = new float[3];

	ColorHsv.rgbToHsv_F32(input, hsvImage);
	ColorHsv.rgbToHsv(color.getRed(), color.getGreen(), color.getBlue(), hsvColor);

	Planar<GrayF32> rgbImage = hsvImage.createSameShape();
	ColorHsv.hsvToRgb_F32(hsvImage, rgbImage);

	GrayF32 H = hsvImage.getBand(0);
	GrayF32 S = hsvImage.getBand(1);
	GrayF32 V = hsvImage.getBand(2);

	GrayU8 result = new GrayU8(input.width, input.height);

	for (int y = 0; y < hsvImage.height; y++) {
	    for (int x = 0; x < hsvImage.width; x++) {

		dh = UtilAngle.dist(H.unsafe_get(x, y), hsvColor[0]);
		dv = Math.abs(V.unsafe_get(x, y) - hsvColor[2]);

		wds = Math.abs(S.unsafe_get(x, y) - hsvColor[1]);
		wdh = (float) (dh / (Math.PI * 2));
		wdv = dv / 255;

		curDist = Math.sqrt(wdh * wdh + wds * wds + wdv * wdv);

		if (curDist <= maxDist) {
		    result.unsafe_set(x, y, 1);
		}
	    }
	}

	GrayS32 labeled = new GrayS32(result.width, result.height);
	return BinaryImageOps.contour(result, ConnectRule.EIGHT, labeled);
    }

    /**
     * Select the maximum perimeter contour
     *
     * @param contours
     *            list of contours
     * @return maximum perimeter contour
     */
    public static Contour getMaximumContour(List<Contour> contours) {
	Contour max = contours.get(0);
	for (Contour contour : contours) {
	    if (contour.external.size() > max.external.size()) {
		max = contour;
	    }
	}
	return max;
    }

    /**
     * Returns center of mass of input contour
     *
     * @param contour
     *            contour to process
     * @return int array, x = int[0], y = int[1]
     */
    public static int[] getCenterMassContour(Contour contour) {
	int xSum = 0;
	int ySum = 0;
	int size = contour.external.size();
	for (Point2D_I32 point : contour.external) {
	    xSum += point.x;
	    ySum += point.y;
	}
	return new int[] { xSum / size, ySum / size };
    }

    /**
     * Select the range of colors for the specified area
     *
     * @param image
     *            image to process
     * @param x
     *            The left point of the rectangle
     * @param y
     *            The top point of the rectangle
     * @param width
     *            Width of the rectangle
     * @param height
     *            Height of the rectangle
     * @return range of the colors array. r1 = int[0], g1 = int[1], b1 = int[2]; r2
     *         = int[3], g2 = int[4], b2 = int[5];
     */
    public static int[] getEndColors(BufferedImage image, int x, int y, int width, int height) {

	final int size = height * width;

	int[] pixels = new int[size];

	image.getRGB(x, y, width, height, pixels, 0, width);

	int[] red = new int[size];
	int[] green = new int[size];
	int[] blue = new int[size];

	for (int i = 0; i < size; i++) {
	    Color c = new Color(pixels[i]);
	    red[i] = c.getRed();
	    green[i] = c.getGreen();
	    blue[i] = c.getBlue();
	}

	int[] result = { red[0], green[0], blue[0], red[0], green[0], blue[0] };

	for (int i = 1; i < size; i++) {
	    result[0] = min(result[0], red[i]);
	    result[1] = min(result[1], green[i]);
	    result[2] = min(result[2], blue[i]);

	    result[3] = max(result[3], red[i]);
	    result[4] = max(result[4], green[i]);
	    result[5] = max(result[5], blue[i]);
	}

	return result;
    }

    /**
     * Select the color of the specified pixel
     *
     * @param image
     *            image to process
     * @param x
     *            x-position of the pixel.
     * @param y
     *            y-position of the pixel.
     * @return color. r = int[0], g = int[1], b = int[2].
     */
    public static int[] getColor(BufferedImage image, int x, int y) {
	int pixel = image.getRGB(x, y);
	Color c = new Color(pixel);
	return new int[] { c.getRed(), c.getGreen(), c.getBlue() };
    }

}
