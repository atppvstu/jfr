package by.vstu.atpp.robotino.action;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * This annotation is used for markup classes witch will be use as Action.
 *
 * @author isap.vstu.by
 *
 */

@Retention(RUNTIME)
@Target(TYPE)
@Component
@Inherited
@Scope(scopeName = "prototype")
public @interface ActionClass {
}
