package by.vstu.atpp.robotino.module;

import by.vstu.atpp.robotino.action.AbstractAction;

/**
 * Module it is a part of the action and used for control the robot according
 * with input data. This class can contain all services for obtaining data from
 * the robot and setting the values of the output ports, robot speed, etc.
 * 
 * @author isap.vstu.by
 *
 */
public interface Module {

    /**
     * Invoked by {@link AbstractAction Action} while Module is working.
     */
    void perform();

    /**
     * @return false if module is still executing
     */
    boolean isFinished();

    /**
     * Set the stop condition of the current module
     * 
     * @param stop
     *            with stop condition
     */
    void setStopCondition(Condition stop);

}