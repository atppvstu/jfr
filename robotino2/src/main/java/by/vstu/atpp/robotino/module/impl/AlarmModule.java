package by.vstu.atpp.robotino.module.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import by.vstu.atpp.robotino.module.AbstractModule;
import by.vstu.atpp.robotino.service.IOService;

/**
 * The module applies a short pulse to the output to which the alarm is
 * connected
 * 
 * @author ZAMOK
 *
 */
@Component
@Scope("prototype")
public class AlarmModule extends AbstractModule {

    @Autowired
    private IOService actuatorService;

    private int counter = 0;
    private int duration = 2;

    /**
     * {@inheritDoc}
     */
    @Override
    public void perform() {
	counter++;
	actuatorService.getDigitalOutputs().get(0).setValue(true);
	if (counter == duration) {
	    actuatorService.getDigitalOutputs().get(0).setValue(false);
	    stop();
	}
    }

    /**
     * @param duration
     *            duration of the alarm.
     */
    public void setDuration(int duration) {
	this.duration = duration;
    }

}
