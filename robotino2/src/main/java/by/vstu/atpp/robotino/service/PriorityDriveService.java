package by.vstu.atpp.robotino.service;

import by.vstu.atpp.robotino.domain.Speed;

/**
 * That class is used to control movement of the robot in two ways: manual
 * control and control from the Action
 *
 * @author isap.vstu.by
 *
 */
public interface PriorityDriveService extends DriveService {

    /**
     * Try set speed to the robot. Manual speed will be set if the robot is not
     * executing the Action
     *
     * @param speed
     *            speed that should be set
     */
    void setManualSpeed(Speed speed);

    /**
     * Switch robot to manual control. In this mode {@link #setManualSpeed(Speed)
     * setManualSpeed(Speed)} sets the speed.
     */
    void setHand();

    /**
     * Switch robot to Action control. In this mode {@link #setManualSpeed(Speed)
     * setManualSpeed(Speed)} method will be ignored.
     */
    void setAuto();

}
