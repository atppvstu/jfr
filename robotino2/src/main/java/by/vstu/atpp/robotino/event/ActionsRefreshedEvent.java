package by.vstu.atpp.robotino.event;

import java.util.EventObject;

/**
 * This is an event that arise when actions was refreshed
 * 
 * @author isap.vstu.by
 *
 */
public class ActionsRefreshedEvent extends EventObject {

    private static final long serialVersionUID = 2719090342777711052L;

    public ActionsRefreshedEvent() {
	super("update");
    }

}
