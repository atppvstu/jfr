package by.vstu.atpp.robotino.service;

import by.vstu.atpp.robotino.domain.Position;
import by.vstu.atpp.robotino.domain.Speed;

/**
 * That class is used to control movement of the robot
 *
 * @author isap.vstu.by
 *
 */
public interface DriveService extends Service {

    /**
     * Set robot speed
     *
     * @param speed
     *            speed that should be setted
     */
    void setSpeed(Speed speed);

    /**
     * @return Imutable copy of setted speed.
     */
    Speed getTargetSpeed();

    /**
     * Set new position value to the odometry.
     *
     * @param position
     *            that should be setted.
     */
    void setOdometry(Position position);

    /**
     * @return Current posision of the robot.
     */
    Position getCurrentPosition();

    /**
     * @return Current speed of the robot.
     */
    Speed getCurrentSpeed();

}
