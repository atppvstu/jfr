package by.vstu.atpp.robotino.robot;

import java.util.ArrayList;
import java.util.List;

import by.vstu.atpp.robotino.robot.listener.BatteryListener;
import rec.robotino.api2.PowerManagement;

/**
 * This class extends PowerManagement class from robotino api2
 * 
 * @author isap.vstu.by
 *
 */
public class DefaultPowerManagement extends PowerManagement {

    private List<BatteryListener> listeners = new ArrayList<>();

    /**
     * Called when battery readings was received
     */
    @Override
    public void readingsEvent(float current, float voltage, boolean charging, int arg3, String batteryType,
	    boolean arg5, int arg6) {
	for (BatteryListener listener : listeners) {
	    listener.onBatteryReceived(current, charging);
	}
    }

    /**
     * Add listener of grappler position
     * 
     * @param listener
     *            listener to add
     */
    public void addListener(BatteryListener listener) {
	listeners.add(listener);
    }

    /**
     * Remove listener of grappler position
     * 
     * @param listener
     *            listener to remove
     */
    public void removeListener(BatteryListener listener) {
	listeners.remove(listener);
    }

}