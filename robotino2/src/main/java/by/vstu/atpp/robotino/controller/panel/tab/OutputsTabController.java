package by.vstu.atpp.robotino.controller.panel.tab;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import by.vstu.atpp.robotino.controller.AbstractController;
import by.vstu.atpp.robotino.service.IOService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;

/**
 * Controller that handles events of the OutputsTab.fxml
 * 
 * @author isap.vstu.by
 *
 */
@Component
public class OutputsTabController extends AbstractController {

    @FXML
    private GridPane grid;

    @Autowired
    private IOService actuatorService;

    private List<CheckBox> outputs;
    private List<CheckBox> relays;
    private OutputChangeHandler outputChangeHandler;
    private RelayChangeHandler relayChangeHandler;

    @FXML
    private void initialize() {
	outputChangeHandler = new OutputChangeHandler();
	relayChangeHandler = new RelayChangeHandler();

	outputs = new ArrayList<>();
	relays = new ArrayList<>();
	CheckBox checkBox;
	ObservableList<Boolean> booleans = FXCollections.observableArrayList();
	booleans.add(false);
	booleans.add(true);
	Label label;
	for (int i = 1; i <= 8; i++) {
	    label = new Label();
	    label.setText("DQ" + i + ": ");
	    grid.add(label, 0, i);
	}
	for (int i = 1; i <= 8; i++) {
	    checkBox = new CheckBox();
	    checkBox.setOnAction(outputChangeHandler);
	    outputs.add(checkBox);
	    grid.add(checkBox, 1, i);
	}
	for (int i = 1; i <= 2; i++) {
	    label = new Label();
	    label.setText("R" + i + ": ");
	    grid.add(label, 2, i);
	}
	for (int i = 1; i <= 2; i++) {
	    checkBox = new CheckBox();
	    checkBox.setOnAction(relayChangeHandler);
	    relays.add(checkBox);
	    grid.add(checkBox, 3, i);
	}
    }

    private class OutputChangeHandler implements EventHandler<ActionEvent> {
	@Override
	public void handle(ActionEvent event) {
	    int outputIndex = outputs.indexOf(event.getSource());
	    actuatorService.getDigitalOutputs().get(outputIndex).setValue(outputs.get(outputIndex).isSelected());
	}
    }

    private class RelayChangeHandler implements EventHandler<ActionEvent> {
	@Override
	public void handle(ActionEvent event) {
	    int relayIndex = relays.indexOf(event.getSource());
	    actuatorService.getRalays().get(relayIndex).setValue(relays.get(relayIndex).isSelected());
	}
    }

}
