package by.vstu.atpp.robotino.controller.panel.tab;

import java.io.PrintStream;

import org.springframework.stereotype.Component;

import by.vstu.atpp.robotino.controller.AbstractController;
import by.vstu.atpp.robotino.util.TextAreaOutputStream;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;

/**
 * Controller that handles events of the ConsoleFrame.fxml
 * 
 * @author isap.vstu.by
 *
 */
@Component
public class ConsoleTabController extends AbstractController {

    @FXML
    private TextArea outputArea;

    @FXML
    private void initialize() {
	TextAreaOutputStream stdStream = new TextAreaOutputStream(outputArea, false);
	TextAreaOutputStream errStream = new TextAreaOutputStream(outputArea, true);
	System.setOut(new PrintStream(stdStream));
	System.setErr(new PrintStream(errStream));
    }

    @FXML
    private void clearConsole() {
	outputArea.clear();
    }
}
