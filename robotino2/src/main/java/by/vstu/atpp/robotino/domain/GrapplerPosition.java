package by.vstu.atpp.robotino.domain;

import rec.robotino.api2.GrapplerReadings;

/**
 *
 * That class defines position of the grappler. GrapplerPosition is immutable
 *
 * @author isap.vstu.by
 *
 */
public class GrapplerPosition {

    private float bottomPosition;
    private float middlePosition;
    private float topPosition;
    private float grapplePosition;

    private float bottomSpeed;
    private float middleSpeed;
    private float topSpeed;
    private float grappleSpeed;

    /**
     * Constructor with default speed. Creates position with passed angles and
     * default speed (1f)
     *
     * @param bottomPosition
     *            position of the bottom servo
     * @param middlePosition
     *            position of the middle servo
     * @param topPosition
     *            position of the top servo
     * @param grapplePosition
     *            position of the gripper servo
     */
    public GrapplerPosition(float bottomPosition, float middlePosition, float topPosition, float grapplePosition) {
	this.bottomPosition = bottomPosition;
	this.middlePosition = middlePosition;
	this.topPosition = topPosition;
	this.grapplePosition = grapplePosition;
	bottomSpeed = 1f;
	middleSpeed = 1f;
	topSpeed = 1f;
	grappleSpeed = 1f;
    }

    /**
     * Constructor with the same speeds on the all axes
     *
     * @param bottomPosition
     *            position of the bottom servo
     * @param middlePosition
     *            position of the middle servo
     * @param topPosition
     *            position of the top servo
     * @param grapplePosition
     *            position of the gripper servo
     * @param speed
     *            speed of the all axis
     */
    public GrapplerPosition(float bottomPosition, float middlePosition, float topPosition, float grapplePosition,
	    float speed) {
	this.bottomPosition = bottomPosition;
	this.middlePosition = middlePosition;
	this.topPosition = topPosition;
	this.grapplePosition = grapplePosition;
	bottomSpeed = speed;
	middleSpeed = speed;
	topSpeed = speed;
	grappleSpeed = speed;
    }

    /**
     * Constructor with different speeds
     *
     * @param bottomPosition
     *            position of the bottom servo
     * @param middlePosition
     *            position of the middle servo
     * @param topPosition
     *            position of the top servo
     * @param grapplePosition
     *            position of the gripper servo
     * @param bottomSpeed
     *            speed of the bottom servo
     * @param middleSpeed
     *            speed of the middle servo
     * @param topSpeed
     *            speed of the top servo
     * @param grappleSpeed
     *            speed of the gripper servo
     */
    public GrapplerPosition(float bottomPosition, float middlePosition, float topPosition, float grapplePosition,
	    float bottomSpeed, float middleSpeed, float topSpeed, float grappleSpeed) {
	this.bottomPosition = bottomPosition;
	this.middlePosition = middlePosition;
	this.topPosition = topPosition;
	this.grapplePosition = grapplePosition;
	this.bottomSpeed = bottomSpeed;
	this.middleSpeed = middleSpeed;
	this.topSpeed = topSpeed;
	this.grappleSpeed = grappleSpeed;
    }

    /**
     * Constructor by GrapplerReadings. Creates GrapplerPosition from data passed in
     * "readings" parameter
     * 
     * @param readings
     *            raw readings
     */
    public GrapplerPosition(GrapplerReadings readings) {
	float[] angles = readings.getAngles();
	float[] speeds = readings.getSpeeds();
	this.bottomPosition = angles[0];
	this.middlePosition = angles[1];
	this.topPosition = angles[2];
	this.grapplePosition = angles[3];
	this.bottomSpeed = speeds[0];
	this.middleSpeed = speeds[1];
	this.topSpeed = speeds[2];
	this.grappleSpeed = speeds[3];
    }

    public float getBottomPosition() {
	return bottomPosition;
    }

    public float getMiddlePosition() {
	return middlePosition;
    }

    public float getTopPosition() {
	return topPosition;
    }

    public float getGrapplePosition() {
	return grapplePosition;
    }

    public float getBottomSpeed() {
	return bottomSpeed;
    }

    public float getMiddleSpeed() {
	return middleSpeed;
    }

    public float getTopSpeed() {
	return topSpeed;
    }

    public float getGrappleSpeed() {
	return grappleSpeed;
    }

    /**
     * Get position of the axis by name
     *
     * @param axis
     *            number of axis
     * @return position of the axis
     */
    public float getAxisPosition(int axis) {
	switch (axis) {
	case 0:
	    return bottomPosition;
	case 1:
	    return middlePosition;
	case 2:
	    return topPosition;
	case 3:
	    return grapplePosition;
	}
	throw new IllegalArgumentException("axis must be ge 0 and le 3");
    }

    /**
     *
     * @param axis
     *            number of axis
     * @return speed of the axis
     */
    public float getAxisSpeed(int axis) {
	switch (axis) {
	case 0:
	    return bottomSpeed;
	case 1:
	    return middleSpeed;
	case 2:
	    return topSpeed;
	case 3:
	    return grappleSpeed;
	}
	throw new IllegalArgumentException("axis must be ge 0 and le 3");
    }

    @Override
    public String toString() {
	StringBuilder builder = new StringBuilder();
	builder.append("GrapplerPosition [bottomPosition=").append(bottomPosition).append(", middlePosition=")
		.append(middlePosition).append(", topPosition=").append(topPosition).append(", grapplePosition=")
		.append(grapplePosition).append(", bottomSpeed=").append(bottomSpeed).append(", middleSpeed=")
		.append(middleSpeed).append(", topSpeed=").append(topSpeed).append(", grappleSpeed=")
		.append(grappleSpeed).append("]");
	return builder.toString();
    }

}
