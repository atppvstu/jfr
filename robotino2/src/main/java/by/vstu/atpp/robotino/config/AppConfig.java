package by.vstu.atpp.robotino.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Spring Java Configuration of the application.
 * 
 * @author isap.vstu.by
 *
 */
@Configuration
@ComponentScan("by.vstu.atpp.robotino")
public class AppConfig {

}
