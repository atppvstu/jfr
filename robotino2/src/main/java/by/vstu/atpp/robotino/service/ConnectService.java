package by.vstu.atpp.robotino.service;

import by.vstu.atpp.robotino.robot.listener.BatteryListener;
import by.vstu.atpp.robotino.robot.listener.ConnectListener;

/**
 * That class is used to control a connection with robot.
 * 
 * @author isap.vstu.by
 *
 */
public interface ConnectService extends Service {

    /**
     * Add {@link BatteryListener listener} to the robot.
     * 
     * @param listener
     *            that will be added
     */
    void addListener(ConnectListener listener);

    /**
     * Connect to the robot.
     * 
     * @param hostname
     *            robot address.
     * @param block
     *            blocking connection
     */
    void connect(String hostname, boolean block);

    /**
     * Disconnect from the current robot
     */
    void disconnect();

    /**
     * @return Status of connection with robot
     */
    boolean isConnected();

}