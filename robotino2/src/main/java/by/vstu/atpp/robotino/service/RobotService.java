package by.vstu.atpp.robotino.service;

import org.springframework.beans.factory.annotation.Autowired;

import by.vstu.atpp.robotino.robot.Robot;

/**
 * This is abstract class for any {@link Service Service}. RobotService store
 * the {@link Robot robot} and provides its to child services.
 * 
 * @author isap.vstu.by
 *
 */
public abstract class RobotService {

    /**
     * Robot that used in child services.
     */
    @Autowired
    protected Robot robot;

    /**
     * Set robot to the service.
     * 
     * @param robot
     *            that should be setted.
     */
    public void setRobot(Robot robot) {
	this.robot = robot;
    }

}
