package by.vstu.atpp.robotino.domain;

/**
 * That class defines position of the robot. Position is immutable
 *
 * @author isap.vstu.by
 *
 */
public class Position {

    public static final Position ZERO_POSITION = new Position();

    private double x;
    private double y;
    private double phi;

    /**
     * @param x
     *            Forward/Back position
     * @param y
     *            Left/Right position
     * @param phi
     *            Turn angle
     */
    public Position(double x, double y, double phi) {
	this.x = x;
	this.y = y;
	this.phi = phi;
    }

    public Position() {

    }

    /**
     * @return Forward-Back position
     */
    public Double getX() {
	return x;
    }

    /**
     * @return Left-Right position
     */
    public Double getY() {
	return y;
    }

    /**
     * @return Turn angle
     */
    public Double getPhi() {
	return phi;
    }

    @Override
    public String toString() {
	StringBuilder builder = new StringBuilder();
	builder.append("Position [x=").append(x).append(", y=").append(y).append(", phi=").append(phi).append("]");
	return builder.toString();
    }

}
