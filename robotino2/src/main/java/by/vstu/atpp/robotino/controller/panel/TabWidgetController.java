package by.vstu.atpp.robotino.controller.panel;

import org.springframework.stereotype.Component;

import by.vstu.atpp.robotino.controller.AbstractController;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 * Controller that handles tab switch events
 *
 * @author isap.vstu.by
 *
 */
@Component
public class TabWidgetController extends AbstractController {

    @FXML
    private Pane tabWidget;
    @FXML
    private TabPane tabPane;
    @FXML
    private VBox headersPane;
    @FXML
    private HBox firstRow;
    @FXML
    private HBox secondRow;

    private ToggleGroup toggleGroup;

    @FXML
    private void initialize() {
	toggleGroup = new ToggleGroup();
	toggleGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
	    @Override
	    public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
		if (newValue == null) {
		    toggleGroup.selectToggle(oldValue);
		} else {
		    tabPane.getSelectionModel().select((Tab) newValue.getUserData());
		}
	    }
	});

	ObservableList<Tab> tabs = tabPane.getTabs();
	int mid = tabs.size() / 2;
	for (int i = 0; i < tabs.size(); i++) {
	    Tab tab = tabs.get(i);
	    ToggleButton tb = new ToggleButton(tab.getText());
	    tab.setText("");
	    tb.setToggleGroup(toggleGroup);
	    tb.setUserData(tab);
	    tb.setMinWidth(70);
	    if (i < mid) {
		firstRow.getChildren().add(tb);
	    } else {
		secondRow.getChildren().add(tb);
	    }
	}

	if (tabs.size() > 0) {
	    toggleGroup.selectToggle(toggleGroup.getToggles().get(0));
	}

    }

}
