package by.vstu.atpp.robotino.service.impl;

import static java.util.Collections.unmodifiableList;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.stereotype.Service;

import by.vstu.atpp.robotino.domain.GrapplerPosition;
import by.vstu.atpp.robotino.robot.listener.StorePositionListener;
import by.vstu.atpp.robotino.service.GrapplerService;
import by.vstu.atpp.robotino.service.RobotService;
import by.vstu.atpp.robotino.util.PathIOUtil;
import by.vstu.atpp.robotino.util.validator.GrapplerValidator;
import by.vstu.atpp.robotino.util.validator.Validator;
import rec.robotino.api2.Grappler;
import rec.robotino.api2.GrapplerReadings;

@Service
public class GrapplerServiceImpl extends RobotService implements GrapplerService {

    private Map<String, GrapplerPosition> savedPositions;
    private Validator<GrapplerPosition> grapplerValidator = new GrapplerValidator();
    private GrapplerPosition currentPosition = new GrapplerPosition(197.07F, 229.10F, 126.69F, 173.61F, 4.5F);
    private boolean power = true;

    public GrapplerServiceImpl() {
	ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
	executorService.scheduleAtFixedRate(() -> {
	    try {
		setGrapplerValues();
	    } catch (Throwable e) {
		e.printStackTrace();
	    }
	}, 100, 300, TimeUnit.MILLISECONDS);
    }

    @PostConstruct
    private void init() {
	savedPositions = PathIOUtil.loadPositionsAsList();
    }

    @PreDestroy
    private void close() {
	PathIOUtil.savePositionsAsList(savedPositions);
    }

    private void setGrapplerValues() throws InterruptedException {
	robot.getGrappler().setPowerEnabled(0, power);
	Thread.sleep(20);
	robot.getGrappler().setPowerEnabled(1, power);
	Thread.sleep(20);
	Grappler grappler = robot.getGrappler();
	float[] angles = grappler.readings().getAngles();
	if (!nearby(angles[0], currentPosition.getBottomPosition())) {
	    robot.getGrappler().setAxis(0, currentPosition.getBottomPosition(), currentPosition.getBottomSpeed());
	    Thread.sleep(400);
	}
	if (!nearby(angles[1], currentPosition.getMiddlePosition())) {
	    robot.getGrappler().setAxis(1, currentPosition.getMiddlePosition(), currentPosition.getMiddleSpeed());
	    Thread.sleep(400);
	}
	if (!nearby(angles[2], currentPosition.getTopPosition())) {
	    robot.getGrappler().setAxis(2, currentPosition.getTopPosition(), currentPosition.getTopSpeed());
	    Thread.sleep(400);
	}
	if (!nearby(angles[3], currentPosition.getGrapplePosition())) {
	    robot.getGrappler().setAxis(3, currentPosition.getGrapplePosition(), currentPosition.getGrappleSpeed());
	    Thread.sleep(400);
	}
    }

    private boolean nearby(float expected, float actual) {
	return expected < actual + 3f && expected > actual - 3f;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setGrapplerPosition(final GrapplerPosition grapplerPosition) {
	currentPosition = grapplerValidator.validate(grapplerPosition);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void toggleGrapplerControl() {
	robot.getGrappler().toggleTorque();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setPowerEnabled(boolean value) {
	power = value;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public GrapplerPosition getSavedPosition(String name) {
	return savedPositions.get(name);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void savePosition(String name, GrapplerPosition position) {
	savedPositions.put(name, position);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteSavedPosition(String name) {
	savedPositions.remove(name);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> getSavedPositionNames() {
	return unmodifiableList(new ArrayList<>(savedPositions.keySet()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public GrapplerPosition getCurrentGrapplerPosition() {
	return new GrapplerPosition(robot.getGrappler().readings());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addStorePositionListener(StorePositionListener listener) {
	robot.addStorePositionListener(listener);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public GrapplerReadings getGrapplerReadings() {
	return robot.getGrappler().readings();
    }

}
