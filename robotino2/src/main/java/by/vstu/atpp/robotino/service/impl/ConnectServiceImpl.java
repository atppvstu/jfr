package by.vstu.atpp.robotino.service.impl;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import javax.annotation.PostConstruct;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import by.vstu.atpp.robotino.robot.listener.ConnectListener;
import by.vstu.atpp.robotino.service.ConnectService;
import by.vstu.atpp.robotino.service.RobotService;

/**
 * This class is default implementation of the {@link ConnectService
 * RobotConnectService}.
 * 
 * @author isap.vstu.by
 *
 */
@Service
public class ConnectServiceImpl extends RobotService implements ConnectService {

    private static final Log LOG = LogFactory.getLog(ConnectServiceImpl.class);

    private boolean needReconnect;

    @PostConstruct
    private void init() {
	ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
	executorService.scheduleAtFixedRate(this::reconnect, 1000, 1000, MILLISECONDS);
    }

    private void reconnect() {
	if (robot.isConnected() || !needReconnect) {
	    return;
	}
	LOG.debug("Trying to reconnect");
	robot.connect(false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addListener(ConnectListener listener) {
	robot.addConnectListener(listener);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void connect(String hostname, boolean block) {
	needReconnect = true;
	robot.setAdddress(hostname);
	robot.connect(block);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void disconnect() {
	needReconnect = false;
	robot.disconnect();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isConnected() {
	return robot.isConnected();
    }

}
