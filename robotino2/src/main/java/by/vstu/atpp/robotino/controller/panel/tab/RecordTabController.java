package by.vstu.atpp.robotino.controller.panel.tab;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Timer;
import java.util.TimerTask;

import javax.imageio.ImageIO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import by.vstu.atpp.robotino.controller.AbstractController;
import by.vstu.atpp.robotino.service.SensorService;
import by.vstu.atpp.robotino.service.UpdatableGuiService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.stage.DirectoryChooser;

/**
 * Controller that handles events of the Record.fxml
 * 
 * @author isap.vstu.by
 *
 */
@Component
public class RecordTabController extends AbstractController {

    @FXML
    private Button selectFolderButton;
    @FXML
    private TextField pathField;
    @FXML
    private Button startRecordButton;
    @FXML
    private Button pauseRecordButton;
    @FXML
    private Button stopRecordButton;
    @FXML
    private CheckBox cameraCB;
    @FXML
    private CheckBox processedCB;

    @Autowired
    private SensorService inputService;
    @Autowired
    private UpdatableGuiService guiService;

    private Timer timer;
    private File tempDir;
    private volatile boolean record;
    private volatile boolean pause;

    @FXML
    private void selectFolder(ActionEvent event) {
	DirectoryChooser chooser = new DirectoryChooser();
	chooser.setTitle("Save Video...");
	File dir = chooser.showDialog(selectFolderButton.getScene().getWindow());
	if (dir != null) {
	    pathField.setText(dir.getAbsolutePath());
	    refreshEnabledButtons();
	}
    }

    private void refreshEnabledButtons() {
	if ((cameraCB.isSelected() || processedCB.isSelected()) && !pathField.getText().isEmpty()) {
	    startRecordButton.setDisable(false);
	    stopRecordButton.setDisable(true);
	    pauseRecordButton.setDisable(true);
	} else {
	    startRecordButton.setDisable(true);
	    stopRecordButton.setDisable(true);
	    pauseRecordButton.setDisable(true);
	}
    }

    @FXML
    private void pathChange(KeyEvent event) {
	refreshEnabledButtons();
    }

    @FXML
    private void startRecord(ActionEvent event) {
	if (!record) {
	    File dir = new File(pathField.getText());
	    if (!dir.exists() || !dir.isDirectory()) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Error!");
		alert.setHeaderText(null);
		alert.setContentText("Directory \"" + pathField.getText() + "\" Not Exists!");
		alert.showAndWait();
		return;
	    }
	    try {
		Path path = Files.createTempDirectory("robotino2");
		tempDir = new File(path.toString());
		timer = new Timer();
		timer.scheduleAtFixedRate(new RecordTask(), 1000, 34);
		record = true;
		pause = false;
		startRecordButton.setDisable(true);
		stopRecordButton.setDisable(false);
		pauseRecordButton.setDisable(false);
		selectFolderButton.setDisable(true);
		pathField.setDisable(true);
		cameraCB.setDisable(true);
		processedCB.setDisable(true);
	    } catch (IOException e) {
		e.printStackTrace();
	    }
	}
	if (pause) {
	    pause = false;
	    startRecordButton.setDisable(true);
	    pauseRecordButton.setDisable(false);
	}
    }

    @FXML
    private void pauseRecord(ActionEvent event) {
	pause = true;
	startRecordButton.setDisable(false);
	pauseRecordButton.setDisable(true);
    }

    @FXML
    private void stopRecord(ActionEvent event) {
	record = false;
	pause = false;
	timer.cancel();
	startRecordButton.setDisable(false);
	stopRecordButton.setDisable(true);
	pauseRecordButton.setDisable(true);
	selectFolderButton.setDisable(false);
	pathField.setDisable(false);
	cameraCB.setDisable(false);
	processedCB.setDisable(false);
	File[] cameraImages = tempDir.listFiles((file, name) -> {
	    return name.startsWith("camera") && name.endsWith(".jpg");
	});
	File[] processedImages = tempDir.listFiles((file, name) -> {
	    return name.startsWith("processed") && name.endsWith(".jpg");
	});
	if (cameraImages.length != 0) {
	    createVideoFromTempImages(cameraImages, "camera");
	}
	if (processedImages.length != 0) {
	    createVideoFromTempImages(processedImages, "processed");
	}
	for (File file : tempDir.listFiles()) {
	    file.delete();
	}
	tempDir.delete();
    }

    private void createVideoFromTempImages(File[] images, String videoName) {
	if (!videoName.endsWith(".mjpeg")) {
	    videoName += ".mjpeg";
	}
	try (DataOutputStream out = new DataOutputStream(
		new BufferedOutputStream(new FileOutputStream(new File(pathField.getText() + "/" + videoName))))) {
	    for (File file : images) {
		DataInputStream in = new DataInputStream(new BufferedInputStream(new FileInputStream(file)));
		while (in.available() > 0) {
		    byte data[] = new byte[in.available()];
		    in.read(data);
		    out.write(data);
		}
		in.close();
	    }
	} catch (IOException e) {
	    e.printStackTrace();
	}
    }

    @FXML
    private void cbChange(ActionEvent event) {
	refreshEnabledButtons();
    }

    private class RecordTask extends TimerTask {

	private int i = 0;
	private int j = 0;

	@Override
	public void run() {
	    if (record && !pause) {
		if (cameraCB.isSelected()) {
		    writeTempJpgImage(inputService.getImage(), "camera", i++);
		}
		if (processedCB.isSelected()) {
		    writeTempJpgImage(guiService.getProcessedImage(), "processed", j++);
		}
	    }
	}

	private void writeTempJpgImage(Image image, String name, int number) {
	    BufferedImage bufferedImage = (BufferedImage) image;
	    if (bufferedImage != null) {
		try {
		    ImageIO.write(bufferedImage, "jpg",
			    new File(tempDir, String.format("%s%06d%s", name, number, ".jpg")));
		} catch (IOException e) {
		    e.printStackTrace();
		}
	    }
	}

    }

}
