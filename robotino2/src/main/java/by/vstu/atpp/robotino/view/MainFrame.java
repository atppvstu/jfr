package by.vstu.atpp.robotino.view;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import by.vstu.atpp.robotino.controller.Controller;
import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * This class is a main frame of the JavaFX Application.
 * 
 * @author iasp.vstu.by
 *
 */
public class MainFrame extends Application {

    /**
     * Start the application.
     */
    @Override
    public void start(Stage primaryStage) {

	try {
	    ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext(
		    "classpath:config/spring-config.xml");

	    FXMLHelper helper = ctx.getBean(FXMLHelper.class);

	    Controller controller = helper.load("/view/MainFrame.fxml");

	    Scene scene = new Scene((Parent) controller.getView());
	    scene.getStylesheets().add("/resources/application.css");
	    primaryStage.setScene(scene);
	    primaryStage.setTitle("Robotino UI");
	    primaryStage.setMinHeight(555);
	    primaryStage.setMinWidth(830);
	    primaryStage.show();

	    primaryStage.setOnCloseRequest(event -> {
		ctx.close();
		System.exit(0);
	    });
	} catch (Exception e) {
	    e.printStackTrace();
	}

    }

}
