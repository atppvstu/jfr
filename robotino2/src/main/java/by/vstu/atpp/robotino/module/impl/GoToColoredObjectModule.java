package by.vstu.atpp.robotino.module.impl;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import boofcv.alg.filter.binary.Contour;
import boofcv.gui.binary.VisualizeBinaryData;
import by.vstu.atpp.robotino.domain.Speed;
import by.vstu.atpp.robotino.module.AbstractModule;
import by.vstu.atpp.robotino.service.DriveService;
import by.vstu.atpp.robotino.service.GuiService;
import by.vstu.atpp.robotino.service.SensorService;
import by.vstu.atpp.robotino.util.ImageProcessUtil;

/**
 * The module moves the robot to the object of the specified color.
 * 
 * @see ImageProcessUtil
 */
@Component
@Scope(scopeName = "prototype")
public class GoToColoredObjectModule extends AbstractModule {

    @Autowired
    private DriveService driveService;
    @Autowired
    private SensorService inputService;
    @Autowired
    private GuiService guiService;

    private float rotateSpeed = 0.7f;
    private float moveSpeed = 0.25f;

    private Color[] colors;
    private float omega;

    // TODO: size
    private static final BufferedImage BLACK_IMAGE = new BufferedImage(640, 480, BufferedImage.TYPE_INT_RGB);

    /**
     * {@inheritDoc}
     */
    @Override
    public void perform() {
	BufferedImage image = (BufferedImage) inputService.getImage();
	int width = image.getWidth();
	int height = image.getHeight();
	Color color = getMediumColor(colors);
	List<Contour> contours = ImageProcessUtil.selectAreasNearColor(image, color, 0.1f);

	if (contours.isEmpty()) {
	    guiService.setProcessedImage(BLACK_IMAGE);
	    driveService.setSpeed(new Speed(moveSpeed, 0, rotateSpeed));
	    return;
	}

	Contour contour = ImageProcessUtil.getMaximumContour(contours);

	int[] centerMass = ImageProcessUtil.getCenterMassContour(contour);
	float dif = image.getWidth() / 2 - centerMass[0];
	omega = dif / height * rotateSpeed;
	driveService.setSpeed(new Speed(moveSpeed, 0, omega));

	BufferedImage outputImage = VisualizeBinaryData.renderContours(contours, Color.MAGENTA.getRGB(),
		Color.BLACK.getRGB(), width, height, null);
	int rectLeft = Math.max(centerMass[0] - 10, 0);
	int rectRight = Math.min(centerMass[0] + 10, width);
	int rectTop = Math.max(centerMass[1] - 10, 0);
	int rectBottom = Math.min(centerMass[1] + 10, width);
	outputImage.getGraphics().drawLine(rectLeft, rectTop, rectRight, rectBottom);
	outputImage.getGraphics().drawLine(rectRight, rectTop, rectLeft, rectBottom);
	guiService.setProcessedImage(outputImage);
    }

    private Color getMediumColor(Color[] colors) {
	int red = 0;
	int green = 0;
	int blue = 0;

	for (Color color : colors) {
	    red += color.getRed();
	    green += color.getGreen();
	    blue += color.getBlue();
	}
	Color result = new Color(red / colors.length, green / colors.length, blue / colors.length);
	return result;
    }

    /**
     * The range of colors to which the robot must move
     * 
     * @param colors
     *            Two border colors
     */
    public void setColors(Color[] colors) {
	this.colors = colors;
    }

    /**
     * 
     * @param rotateSpeed
     *            The robot rotation speed
     */
    public void setRotateSpeed(float rotateSpeed) {
	this.rotateSpeed = rotateSpeed;
    }

    /**
     * @param moveSpeed
     *            The robot moving speed
     */
    public void setMoveSpeed(float moveSpeed) {
	this.moveSpeed = moveSpeed;
    }

}
