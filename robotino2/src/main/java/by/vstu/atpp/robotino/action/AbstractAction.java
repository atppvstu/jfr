package by.vstu.atpp.robotino.action;

import static by.vstu.atpp.robotino.util.Speeds.ZERO_SPEED;
import static com.google.common.util.concurrent.MoreExecutors.listeningDecorator;
import static java.util.concurrent.Executors.newSingleThreadScheduledExecutor;
import static java.util.concurrent.TimeUnit.MILLISECONDS;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.util.concurrent.ListenableScheduledFuture;
import com.google.common.util.concurrent.ListeningScheduledExecutorService;

import by.vstu.atpp.robotino.module.Module;
import by.vstu.atpp.robotino.service.DriveService;
import by.vstu.atpp.robotino.service.PriorityDriveService;
import by.vstu.atpp.robotino.service.SensorService;

/**
 * Basic class for any action from modules. If you want to make your own action
 * you should create a new class with AbstractAction as a superclass and add
 * modules to the modules sequence use method {@link #addLast(Module) addLast}.
 * For example:
 *
 * <pre>
 * <code>
 * public class MyAction extends AbstractAction {
 *
 * 	public MyAction(SomeModule module) {
 * 		addLast(module);
 * 	}
 * }
 * }
 * </code>
 * </pre>
 *
 * @author isap.vstu.by
 * @see Module
 * @see ActionManager
 */
public abstract class AbstractAction implements Action {

    @Autowired
    private SensorService inputService;
    @Autowired
    private PriorityDriveService driveService;

    private int currentModuleNumber = 0;
    private ListeningScheduledExecutorService service = listeningDecorator(newSingleThreadScheduledExecutor());
    private ListenableScheduledFuture<?> future;
    private List<Module> modules = new ArrayList<>();

    /**
     * it's a main method in the AbstractAction. This method invoked by the
     * {@link ActionManager ActionManager} to start the action. {@link #initHook()
     * initHook} method will be invoked before any module will be executed and
     * manual control in {@link DriveService DriveService} will be disabled.
     *
     * @return ListenableScheduledFuture - for check end of the action or cansel it.
     */
    @Override
    public final ListenableScheduledFuture<?> execute() {
	future = service.scheduleWithFixedDelay(this::process, 0, getDelay(), MILLISECONDS);
	driveService.setAuto();
	initHook();
	return future;
    }

    private void process() {
	if (inputService.isBumpered()) {
	    cancel();
	    return;
	}
	Module module = modules.get(currentModuleNumber);
	tryPerform(module);
	afterEach();
	if (module.isFinished()) {
	    currentModuleNumber++;
	    driveService.setSpeed(ZERO_SPEED);
	}
	if (currentModuleNumber >= modules.size()) {
	    afterLast();
	}
    }

    private void tryPerform(Module module) {
	try {
	    module.perform();
	} catch (Exception exception) {
	    cancel();
	    exception.printStackTrace();
	} catch (Error error) {
	    cancel();
	    System.err.println("Try to add a library from the error in the folder \"documents/robotino2/extLib\"");
	    error.printStackTrace();
	}
    }

    /**
     * Cancel action. Invoked by the {@link ActionManager ActionManager} to cancel
     * the action. {@link #cancelHook() canselHook} method will be invoked and
     * manual control in {@link DriveService DriveService} will be enabled.
     */
    @Override
    public final void cancel() {
	if (future != null) {
	    future.cancel(true);
	}
	driveService.setSpeed(ZERO_SPEED);
	driveService.setHand();
	cancelHook();
    }

    /**
     * Set number of the module, which will be executed next one.
     * 
     * @param number
     *            Number in order
     */
    public final void setModuleNumber(int number) {
	if (number < 0) {
	    currentModuleNumber = 0;
	}
	if (number > modules.size() - 1) {
	    currentModuleNumber = modules.size() - 1;
	}
    }

    /**
     * Invoked after the action has been stopped. Use this method to close all
     * resources that was opened in the {@link #initHook() initHook} method.
     */
    protected void cancelHook() {

    }

    /**
     * Invoked before action will be started. Use this method to open any required
     * resources.
     */
    protected void initHook() {

    }

    /**
     * Add {@link Module module} in the end of the modules sequence.
     *
     * @param module
     *            module to adding
     */
    protected void addLast(Module module) {
	modules.add(module);
    }

    protected void afterEach() {
	// Do nothing
    }

    /**
     * Invoked when all modules in the modules sequence are finished. You can
     * override value of the {@link #currentModuleNumber numberAction} field for
     * continue the action from exactly {@link #currentModuleNumber numberAction}
     * module.
     */
    protected void afterLast() {
	cancel();
    }

    /**
     * Returns delay between invokes {@link Module#perform() perform} method of a
     * current module. You can overcride this method to use your own delay.
     *
     * @return delay between invokes {@link Module#perform() perform} method of
     *         current module in milliseconds. 100 milliseconds as default.
     */
    protected int getDelay() {
	return 100;
    }

}
