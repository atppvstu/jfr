package by.vstu.atpp.robotino.controller.panel;

import static java.lang.Math.abs;
import static java.lang.Math.min;

import java.awt.Point;
import java.awt.image.BufferedImage;

import org.springframework.stereotype.Component;

import by.vstu.atpp.robotino.controller.AbstractController;
import by.vstu.atpp.robotino.util.ImageProcessUtil;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Rectangle;

/**
 * Controller that handles events of the Record.fxml
 * 
 * @author isap.vstu.by
 *
 */
@Component
public class ImageWidgetController extends AbstractController {

    private static final int IMAGE_WIDTH = 320;
    private static final int IMAGE_HEIGHT = 240;
    private static final double RATIO = 0.75;

    @FXML
    private Pane imagePane;
    @FXML
    private ImageView imageView;
    @FXML
    private Rectangle select;
    @FXML
    private Rectangle gradient;
    @FXML
    private Rectangle pixelZoom;

    private double width1, height1, width2, height2;

    private Point point;
    private Color pixelColor;
    private Color color1;
    private Color color2;

    @FXML
    private void initialize() {
	Image image = new Image("resources/nowifi.png");
	imageView.setImage(image);

	point = new Point();
	pixelColor = new Color(0, 0, 0, 0);
	color1 = new Color(0, 0, 0, 0);
	color2 = new Color(0, 0, 0, 0);

	ChangeListener<Number> changeListener = new ChangeListener<Number>() {
	    @Override
	    public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
		onResize();
	    }
	};
	imagePane.widthProperty().addListener(changeListener);
	imagePane.heightProperty().addListener(changeListener);
	onResize();
    }

    private void onResize() {
	width1 = imagePane.getWidth();
	height1 = width1 * RATIO;

	height2 = imagePane.getHeight() - 75;
	width2 = height2 / RATIO;

	if (height1 < height2) {
	    imageView.setFitWidth(width1);
	    imageView.setFitHeight(height1);
	    imageView.setX(0);
	    imageView.setY((height2 - height1) / 2);
	} else {
	    imageView.setFitWidth(width2);
	    imageView.setFitHeight(height2);
	    imageView.setY(0);
	    imageView.setX((width1 - width2) / 2);
	}

	gradient.setY(imageView.getY() + imageView.getFitHeight() + 5);
	gradient.setX(imageView.getX());
	gradient.setWidth(imageView.getFitWidth());

	pixelZoom.setY(gradient.getY() + gradient.getHeight() + 5);
	pixelZoom.setX(imageView.getX());
    }

    @FXML
    private void mousePressed(MouseEvent event) {
	if (MouseButton.PRIMARY.equals(event.getButton())) {
	    if (event.isControlDown()) {

		double relX = event.getX() - imageView.getX();
		double relY = event.getY() - imageView.getY();

		double absX = relX / imageView.getFitWidth() * IMAGE_WIDTH;
		double absY = relY / imageView.getFitHeight() * IMAGE_HEIGHT;

		int[] colors = ImageProcessUtil.getColor(SwingFXUtils.fromFXImage(imageView.getImage(), null),
			(int) absX, (int) absY);
		pixelColor = createColor(colors[0], colors[1], colors[2]);
		pixelZoom.setFill(pixelColor);
	    } else {
		point.setLocation(event.getX(), event.getY());
		select.setWidth(0);
		select.setHeight(0);
		select.setVisible(true);
	    }
	}
    }

    private Color createColor(double r, double g, double b) {
	return new Color(r / 255, g / 255, b / 255, 1.0);
    }

    @FXML
    private void mouseDragged(MouseEvent event) {
	final double x = event.getX();
	final double y = event.getY();

	if (x > imageView.getX() && x < imageView.getX() + imageView.getFitWidth()) {
	    select.setX(min(x, point.getX()));
	    select.setWidth(abs(x - point.getX()));
	}
	if (y > imageView.getY() && y < imageView.getY() + imageView.getFitHeight()) {
	    select.setY(min(y, point.getY()));
	    select.setHeight(abs(y - point.getY()));
	}
    }

    @FXML
    private void mouseReleased(MouseEvent event) {
	if (MouseButton.PRIMARY.equals(event.getButton()) && select.getWidth() > 0 && select.getHeight() > 0) {

	    double relX = select.getX() - imageView.getX();
	    double relY = select.getY() - imageView.getY();

	    double absX = relX / imageView.getFitWidth() * IMAGE_WIDTH;
	    double absY = relY / imageView.getFitHeight() * IMAGE_HEIGHT;

	    int[] colors = ImageProcessUtil.getEndColors(SwingFXUtils.fromFXImage(imageView.getImage(), null),
		    (int) absX, (int) absY, (int) select.getWidth(), (int) select.getHeight());
	    color1 = createColor(colors[0], colors[1], colors[2]);
	    color2 = createColor(colors[3], colors[4], colors[5]);

	    Stop[] stops = new Stop[] { new Stop(0, color1), new Stop(1, color2) };
	    LinearGradient lg1 = new LinearGradient(0, 0, 1, 0, true, CycleMethod.NO_CYCLE, stops);
	    gradient.setFill(lg1);
	    select.setVisible(false);
	}
    }

    /**
     * @return Color that was choise by user when ctrl button was perssed.
     */
    public java.awt.Color getSingleColor() {
	return convertColor(pixelColor);
    }

    private java.awt.Color convertColor(Color color) {
	return new java.awt.Color((float) color.getRed(), (float) color.getGreen(), (float) color.getBlue());
    }

    /**
     * That method draw image on GUI
     * 
     * @param image
     *            image that will be drawn
     */
    public void drawImage(java.awt.Image image) {
	if (image != null) {
	    imageView.setImage(SwingFXUtils.toFXImage((BufferedImage) image, null));
	}
    }

    /**
     * @return Border colors witch was choise by user
     */
    public java.awt.Color[] getRangeColor() {
	return new java.awt.Color[] { convertColor(color1), convertColor(color2) };
    }

}
