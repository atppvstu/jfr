package by.vstu.atpp.robotino.service.impl;

import java.util.List;

import org.springframework.stereotype.Component;

import by.vstu.atpp.robotino.service.IOService;
import by.vstu.atpp.robotino.service.RobotService;
import rec.robotino.api2.AnalogInput;
import rec.robotino.api2.DigitalInput;
import rec.robotino.api2.DigitalOutput;
import rec.robotino.api2.Relay;

/**
 * Default implementation of the {@link IOService RobotActuatorService}.
 *
 * @author isap.vstu.by
 *
 */
@Component
public class IOServiceImpl extends RobotService implements IOService {

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Relay> getRalays() {
	return robot.getRelays();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<DigitalOutput> getDigitalOutputs() {
	return robot.getDigitalOutputs();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<DigitalInput> getDigitalInputs() {
	return robot.getDigitalInputs();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<AnalogInput> getAnalogInputs() {
	return robot.getAnalogInputs();
    }

}
