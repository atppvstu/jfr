package by.vstu.atpp.robotino.robot.listener;

public interface PositionListener {

    /**
     * Invokes when odometry data will be received from the robot.
     * 
     * @param x
     *            Forward/Back position
     * @param y
     *            Left/Right position
     * @param phi
     *            Turn angle
     * @param vx
     *            Forward/Back speed
     * @param vy
     *            Left/Right speed
     * @param omega
     *            Turn speed
     */
    void onOdometryReceived(double x, double y, double phi, float vx, float vy, float omega);

}
