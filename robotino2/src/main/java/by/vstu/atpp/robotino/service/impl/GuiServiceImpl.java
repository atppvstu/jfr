package by.vstu.atpp.robotino.service.impl;

import java.awt.Color;
import java.awt.Image;

import org.springframework.stereotype.Service;

import by.vstu.atpp.robotino.service.GuiService;
import by.vstu.atpp.robotino.service.UpdatableGuiService;

/**
 * This class is default implementation of the {@link GuiService
 * UserInputService}.
 * 
 * @author isap.vstu.by
 *
 */
@Service
public class GuiServiceImpl implements UpdatableGuiService {

    private volatile Image image;
    private volatile float sliderSpeed;
    private volatile Color[] rangeColor;
    private volatile Color singleColor;

    /**
     * {@inheritDoc}
     */
    @Override
    public Image getProcessedImage() {
	return image;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setProcessedImage(Image image) {
	this.image = image;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public float getSliderSpeed() {
	return sliderSpeed;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setSliderSpeed(float sliderSpeed) {
	this.sliderSpeed = sliderSpeed;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Color[] getRangeColor() {
	return rangeColor;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setRangeColor(Color[] rangeColor) {
	this.rangeColor = rangeColor;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Color getSingleColor() {
	return singleColor;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setSingleColor(Color singleColor) {
	this.singleColor = singleColor;
    }

}
