package by.vstu.atpp.robotino.service.impl;

import static by.vstu.atpp.robotino.controller.panel.tab.InputsTabController.FALSE_VALUES;
import static by.vstu.atpp.robotino.controller.panel.tab.InputsTabController.ZERO_VALUES;
import static by.vstu.atpp.robotino.controller.panel.tab.ParktronicsTabController.MAX_VALUES;
import static by.vstu.atpp.robotino.domain.BatteryStatus.OFFLINE;
import static by.vstu.atpp.robotino.domain.Position.ZERO_POSITION;
import static by.vstu.atpp.robotino.util.Speeds.ZERO_SPEED;
import static by.vstu.atpp.robotino.view.ViewUtil.loadImage;
import static java.util.concurrent.TimeUnit.MILLISECONDS;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import by.vstu.atpp.robotino.controller.panel.ImageWidgetController;
import by.vstu.atpp.robotino.controller.panel.tab.BatteryTabController;
import by.vstu.atpp.robotino.controller.panel.tab.GrapplerTabController;
import by.vstu.atpp.robotino.controller.panel.tab.InputsTabController;
import by.vstu.atpp.robotino.controller.panel.tab.OdometryTabController;
import by.vstu.atpp.robotino.controller.panel.tab.ParktronicsTabController;
import by.vstu.atpp.robotino.controller.panel.tab.ProcessedImageTabController;
import by.vstu.atpp.robotino.domain.GrapplerPosition;
import by.vstu.atpp.robotino.service.ConnectService;
import by.vstu.atpp.robotino.service.DriveService;
import by.vstu.atpp.robotino.service.GrapplerService;
import by.vstu.atpp.robotino.service.IOService;
import by.vstu.atpp.robotino.service.SensorService;

/**
 * This class is used for transferring data from services to GUI
 * 
 * @author isap.vstu.by
 */
@Service
public class ViewUpdaterService {

    @Autowired
    private ImageWidgetController imageWidgetController;
    @Autowired
    private ParktronicsTabController parktronicsTabController;
    @Autowired
    private InputsTabController inputsTabController;
    @Autowired
    private OdometryTabController odometryTabController;
    @Autowired
    private BatteryTabController batteryTabController;
    @Autowired
    private GrapplerTabController grapplerTabController;
    @Autowired
    private ProcessedImageTabController processedImageTabController;

    @Autowired
    private SensorService sensorService;
    @Autowired
    private ConnectService connectService;
    @Autowired
    private GuiServiceImpl guiService;
    @Autowired
    private DriveService driveService;
    @Autowired
    private IOService ioService;
    @Autowired
    private GrapplerService grapplerService;

    @PostConstruct
    private void init() {
	ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
	executorService.scheduleAtFixedRate(this::updateView, 1500, 20, MILLISECONDS);
    }

    private void updateView() {
	try {
	    updateGuiServiceData();
	    if (connectService.isConnected()) {
		updateGuiFromInputService();
	    } else {
		updateGuiFromStubs();
	    }
	} catch (Throwable e) {
	    e.printStackTrace();
	}
    }

    private void updateGuiServiceData() {
	guiService.setRangeColor(imageWidgetController.getRangeColor());
	guiService.setSingleColor(imageWidgetController.getSingleColor());
	guiService.setSliderSpeed(odometryTabController.getSliderSpeed());
    }

    private void updateGuiFromInputService() {
	double[] distances = new double[9];
	for (int i = 0; i < distances.length; i++) {
	    distances[i] = sensorService.getSensors().get(i).distance();
	}
	parktronicsTabController.setDistance(distances);

	double[] analogInputs = new double[8];
	for (int i = 0; i < analogInputs.length; i++) {
	    analogInputs[i] = ioService.getAnalogInputs().get(i).value();
	}
	inputsTabController.setAnalogValues(analogInputs);

	boolean[] digitalInputs = new boolean[8];
	for (int i = 0; i < digitalInputs.length; i++) {
	    digitalInputs[i] = ioService.getDigitalInputs().get(i).value();
	}
	inputsTabController.setDigitalValues(digitalInputs);

	odometryTabController.setSpeed(driveService.getCurrentSpeed());
	odometryTabController.setPosition(driveService.getCurrentPosition());
	batteryTabController.setBatteryStatus(sensorService.getBatteryStatus());

	float[] grapplerAngles = grapplerService.getGrapplerReadings().getAngles();
	GrapplerPosition grapplerPosition = new GrapplerPosition(grapplerAngles[0], grapplerAngles[1],
		grapplerAngles[2], grapplerAngles[3]);

	grapplerTabController.setGrapplerPosition(grapplerPosition);
	imageWidgetController.drawImage(sensorService.getImage());
	processedImageTabController.drawImage(guiService.getProcessedImage());
    }

    private void updateGuiFromStubs() {
	parktronicsTabController.setDistance(MAX_VALUES);
	batteryTabController.setBatteryStatus(OFFLINE);
	odometryTabController.setPosition(ZERO_POSITION);
	odometryTabController.setSpeed(ZERO_SPEED);
	inputsTabController.setAnalogValues(ZERO_VALUES);
	inputsTabController.setDigitalValues(FALSE_VALUES);

	imageWidgetController.drawImage(loadImage("/resources/nowifi.png"));
	processedImageTabController.drawImage(loadImage("/resources/nowifi.png"));
    }

}
