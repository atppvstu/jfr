package by.vstu.atpp.robotino.controller.panel.tab;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import by.vstu.atpp.robotino.controller.AbstractController;
import by.vstu.atpp.robotino.domain.BatteryStatus;
import by.vstu.atpp.robotino.service.ShutdownService;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;

/**
 * Controller that handles events of the BatteryTab.fxml
 * 
 * @author isap.vstu.by
 *
 */
@Component
public class BatteryTabController extends AbstractController {

    @FXML
    private Label voltageLabel;
    @FXML
    private Label chargingLabel;

    @Autowired
    private ShutdownService shutdownService;

    /**
     * Show value of the battery status on the BatteryTab
     * 
     * @param batteryStatus
     *            status value that will be shown
     */
    public void setBatteryStatus(BatteryStatus batteryStatus) {
	Platform.runLater(() -> {
	    voltageLabel.setText(String.format("%.3f", batteryStatus.getCurrent()));
	    chargingLabel.setText(String.valueOf(batteryStatus.isCharging()));
	});
    }

    @FXML
    private void shutdown() {
	Alert alert = new Alert(AlertType.CONFIRMATION);
	alert.setTitle("Shutdown");
	alert.setHeaderText("Do you really want to shutdown robot?");

	Optional<ButtonType> result = alert.showAndWait();
	if (result.isPresent() && result.get() == ButtonType.OK) {
	    shutdownService.shutdown();
	}
    }

}
