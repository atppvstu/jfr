package by.vstu.atpp.robotino.service.impl;

import javax.annotation.PostConstruct;
import javax.swing.Timer;

import org.springframework.stereotype.Service;

import by.vstu.atpp.robotino.domain.Position;
import by.vstu.atpp.robotino.domain.Speed;
import by.vstu.atpp.robotino.service.DriveService;
import by.vstu.atpp.robotino.service.PriorityDriveService;
import by.vstu.atpp.robotino.service.RobotService;
import by.vstu.atpp.robotino.util.Speeds;
import by.vstu.atpp.robotino.util.validator.DefaultSpeedValidator;
import by.vstu.atpp.robotino.util.validator.Validator;

/**
 * This class is default implementation of the {@link DriveService
 * DriveService}.
 * 
 * @author isap.vstu.by
 *
 */
@Service
public class DriveServiceImpl extends RobotService implements PriorityDriveService {

    private volatile Speed currentSpeed = Speeds.ZERO_SPEED;
    private volatile Position currentPosition = Position.ZERO_POSITION;
    private volatile Speed target = Speeds.ZERO_SPEED;
    private Validator<Speed> validator = new DefaultSpeedValidator();
    private boolean isAuto = false;
    private Timer timer;

    @PostConstruct
    private void init() {
	timer = new Timer(20, action -> move());
	timer.start();
	robot.addPositionListener((x, y, phi, vx, vy, omega) -> {
	    currentPosition = new Position(x, y, phi);
	    currentSpeed = new Speed(vx, vy, omega);
	});
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Speed getTargetSpeed() {
	return target;
    }

    private void move() {
	robot.setVelocity(target.getVx(), target.getVy(), target.getOmega());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setAuto() {
	isAuto = true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setHand() {
	isAuto = false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setManualSpeed(Speed speed) {
	if (!isAuto) {
	    target = validator.validate(speed);
	}
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setSpeed(Speed speed) {
	if (isAuto) {
	    target = validator.validate(speed);
	}
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setOdometry(Position position) {
	robot.getOdometry().set(position.getX(), position.getY(), position.getPhi());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Position getCurrentPosition() {
	return currentPosition;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Speed getCurrentSpeed() {
	return currentSpeed;
    }

}
