package by.vstu.atpp.robotino.action;

import com.google.common.util.concurrent.ListenableScheduledFuture;

import by.vstu.atpp.robotino.module.Module;
import by.vstu.atpp.robotino.service.DriveService;
import by.vstu.atpp.robotino.service.GuiService;
import by.vstu.atpp.robotino.service.IOService;
import by.vstu.atpp.robotino.service.SensorService;

/**
 * Action is a central class of the JavaFrameworkForRobotino. The action is used
 * to control the robot. This class can contain all services for obtaining data
 * from the robot and setting the values of the output ports, robot speed, etc.
 * 
 * @author isap.vstu.by
 *
 * @see AbstractAction
 * @see Module
 * @see SensorService
 * @see DriveService
 * @see IOService
 * @see GuiService
 */
public interface Action {

    /**
     * it's a main method in the Action. This method invoked by the
     * {@link ActionManager ActionManager} to start the action.
     * 
     * @return ListenableScheduledFuture - for check end of the action or cancel it.
     */
    ListenableScheduledFuture<?> execute();

    /**
     * Cancel action. Invoked by the {@link ActionManager ActionManager} to cancel
     * the action.
     */
    void cancel();
}
