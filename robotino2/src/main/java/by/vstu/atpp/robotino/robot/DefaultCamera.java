package by.vstu.atpp.robotino.robot;

import java.awt.Image;
import java.util.ArrayList;
import java.util.List;

import by.vstu.atpp.robotino.robot.listener.ImageListener;
import rec.robotino.api2.Camera;

/**
 * This class extends Camera class from robotino api2
 * 
 * @author isap.vstu.by
 *
 */
public class DefaultCamera extends Camera {

    private List<ImageListener> listeners = new ArrayList<>();

    /**
     * Default constructor
     */
    public DefaultCamera() {
	setCameraNumber(0);
	setBGREnabled(false);
    }

    /**
     * Called when image was received from robot
     */
    @Override
    public void imageReceivedEvent(Image img, long dataSize, long width, long height, long step) {
	for (ImageListener listener : listeners) {
	    listener.onImageReceived(img);
	}
    }

    /**
     * Add listener of image receiving
     * 
     * @param listener
     *            listener to add
     */
    public void addListener(ImageListener listener) {
	listeners.add(listener);
    }

    /**
     * Remove listener of image receiving
     * 
     * @param listener
     *            listener to remove
     */
    public void removeListener(ImageListener listener) {
	listeners.remove(listener);
    }
}