package by.vstu.atpp.robotino.service;

import java.awt.Image;
import java.util.List;

import by.vstu.atpp.robotino.domain.BatteryStatus;
import rec.robotino.api2.DistanceSensor;

/**
 * This service is used to obtaining information from robot.
 * 
 * @author isap.vstu.by
 *
 */
public interface SensorService extends Service {

    /**
     * @return Image from camera.
     */
    Image getImage();

    /**
     * @return List of distance sensors located along the perimeter of the robot.
     */
    List<DistanceSensor> getSensors();

    /**
     * @return Battery voltage and charging status
     */
    BatteryStatus getBatteryStatus();

    /**
     * @return True if bumper is bent.
     */
    boolean isBumpered();

}
