package by.vstu.atpp.robotino.loader;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

import by.vstu.atpp.robotino.module.ModulePackage;

/**
 * This class is used to load and define classes from serialized ModulePackages.
 * 
 * @author isap.vstu.by
 *
 */
public class FileClassLoader extends ClassLoader {

    /**
     * Load class from file and serialize them.
     * 
     * @param file
     *            file with serialized ModulePackage
     * @return defined class.
     * @throws ClassNotFoundException
     *             if file is not found.
     */
    public Class<?> loadClass(File file) throws ClassNotFoundException {
	ModulePackage pack = loadPackage(file);
	if (pack != null) {
	    byte[] array = pack.getClassByteArray();
	    return defineClass(pack.getModuleClass(), array, 0, array.length);
	}
	throw new ClassNotFoundException();
    }

    private ModulePackage loadPackage(File file) throws ClassNotFoundException {
	try (ObjectInputStream stream = new ObjectInputStream(new FileInputStream(file));) {
	    return (ModulePackage) stream.readObject();
	} catch (IOException e) {
	}
	return null;
    }

}
