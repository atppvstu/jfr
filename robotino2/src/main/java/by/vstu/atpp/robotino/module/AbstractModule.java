package by.vstu.atpp.robotino.module;

/**
 * Module it is a part of the action and used for control the robot according
 * with input data. This class can contain all services for obtaining data from
 * the robot and setting the values of the output ports, robot speed, etc. For
 * example:
 * 
 * <pre>
 * <code>
 * public class WallFollowModule extends AbstractModule {

	private DriveService driveService;
	private DataInputService inputService;

	&#64;Override
	public void perform() {
		if (inputService.getSensors().get(0).distance() &lt; 0.1f) {
			driveService.setSpeed(new Speed(0.1f, 0, 0));
		} else {
			stop();
		}
	}

}

 * </code>
 * </pre>
 * 
 * @author isap.vstu.by
 *
 */
public abstract class AbstractModule implements Module {

    private Condition stopCondition;

    private boolean finished;

    /**
     * {@inheritDoc}
     */
    @Override
    public abstract void perform();

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isFinished() {
	if (finished) {
	    finished = false;
	    return true;
	}
	return stopCondition != null && stopCondition.test();
    }

    /**
     * Stop the current module
     */
    protected final void stop() {
	finished = true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setStopCondition(Condition stopCondition) {
	this.stopCondition = stopCondition;
    }

}
