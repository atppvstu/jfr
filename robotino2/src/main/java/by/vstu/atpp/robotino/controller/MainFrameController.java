package by.vstu.atpp.robotino.controller;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import by.vstu.atpp.robotino.controller.panel.tab.OdometryTabController;
import by.vstu.atpp.robotino.domain.Speed;
import by.vstu.atpp.robotino.service.PriorityDriveService;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;

/**
 * Controller that handles events of the MainFrame.fxml
 * 
 * @author isap.vstu.by
 *
 */
@Component
public class MainFrameController extends AbstractController {

    private static final Set<KeyCode> KEYS = new HashSet<>();
    @FXML
    private BorderPane borderPane;

    @Autowired
    private PriorityDriveService driveService;
    @Autowired
    private OdometryTabController odometryTabController;

    @FXML
    private void initialize() {
	Platform.runLater(() -> {
	    borderPane.getScene().getWindow().focusedProperty().addListener((obs, oldValue, newValue) -> {
		KEYS.clear();
		refreshSpeed();
	    });
	});
    }

    private void refreshSpeed() {
	float vx = 0;
	float vy = 0;
	float omega = 0;
	float defSpeed = odometryTabController.getSliderSpeed();
	for (KeyCode keyCode : KEYS) {
	    switch (keyCode) {
	    case A:
		vy += defSpeed;
		break;
	    case D:
		vy -= defSpeed;
		break;
	    case W:
		vx += defSpeed;
		break;
	    case S:
		vx -= defSpeed;
		break;
	    case Q:
		omega += defSpeed * 2;
		break;
	    case E:
		omega -= defSpeed * 2;
		break;
	    default:
		break;
	    }
	}
	driveService.setManualSpeed(new Speed(vx, vy, omega));
    }

    @FXML
    private void keyPressed(KeyEvent event) {
	if (!KEYS.contains(event.getCode())) {
	    KEYS.add(event.getCode());
	    refreshSpeed();
	}
    }

    @FXML
    private void keyReleased(KeyEvent event) {
	KEYS.remove(event.getCode());
	refreshSpeed();
    }

}
