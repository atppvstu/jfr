package by.vstu.atpp.robotino.domain;

/**
 * That class defines speed of the robot. Speed is immutable
 *
 * @author isap.vstu.by
 *
 */
public final class Speed {

    private float vx;
    private float vy;
    private float omega;

    /**
     *
     * @param vx
     *            Forward/Back speed
     * @param vy
     *            Left/Right speed
     * @param omega
     *            Turn speed
     */
    public Speed(float vx, float vy, float omega) {
	this.vx = vx;
	this.vy = vy;
	this.omega = omega;
    }

    /**
     * @return Forward/Back speed
     */
    public Float getVx() {
	return vx;
    }

    /**
     * @return Left/Right speed
     */
    public Float getVy() {
	return vy;
    }

    /**
     *
     * @return Turn speed
     */
    public Float getOmega() {
	return omega;
    }

    @Override
    public String toString() {
	StringBuilder builder = new StringBuilder();
	builder.append("Speed [vx=").append(vx).append(", vy=").append(vy).append(", omega=").append(omega).append("]");
	return builder.toString();
    }

}
