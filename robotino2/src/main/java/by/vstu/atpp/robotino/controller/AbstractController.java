package by.vstu.atpp.robotino.controller;

import javafx.scene.Node;

/**
 * This class handles the JavaFX Node events.
 * 
 * @author isap.vstu.by
 *
 */
public class AbstractController implements Controller {

    private Node node;

    /**
     * @return JavaFX Node stored in this controller
     */
    @Override
    public Node getView() {
	return node;
    }

    /**
     * @param view
     *            Node that will be stored in this controller
     * 
     */
    @Override
    public void setView(Node view) {
	node = view;
    }

}
