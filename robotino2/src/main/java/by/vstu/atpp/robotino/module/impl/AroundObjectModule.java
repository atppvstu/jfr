package by.vstu.atpp.robotino.module.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import by.vstu.atpp.robotino.domain.Speed;
import by.vstu.atpp.robotino.module.AbstractModule;
import by.vstu.atpp.robotino.service.SensorService;
import by.vstu.atpp.robotino.service.DriveService;

/**
 * The module moves the robot around the object
 * 
 * @author isap.vstu.by
 *
 */
@Component
@Scope(scopeName = "prototype")
public class AroundObjectModule extends AbstractModule {

    @Autowired
    private DriveService driveService;
    @Autowired
    private SensorService inputService;

    private float rotateSpeed = 0.3f;
    private float moveSpeed = 0.1f;
    private float distance = 0.1f;

    private float omega;

    /**
     * {@inheritDoc}
     */
    @Override
    public void perform() {
	float curDistance = getDistance(7);

	if (curDistance < distance) {
	    omega = rotateSpeed;
	} else {
	    omega = -rotateSpeed;
	}
	if (Math.abs(curDistance - distance) < 0.02) {
	    omega = 0;
	}
	driveService.setSpeed(new Speed(moveSpeed, 0, omega));
    }

    private float getDistance(int num) {
	return inputService.getSensors().get(num).distance();
    }

    /**
     * @param rotateSpeed
     *            The robot rotation speed.
     */
    public void setRotateSpeed(float rotateSpeed) {
	this.rotateSpeed = rotateSpeed;
    }

    /**
     * @param moveSpeed
     *            The robot moving speed.
     */
    public void setMoveSpeed(float moveSpeed) {
	this.moveSpeed = moveSpeed;
    }

    /**
     * @param distance
     *            The distance at which the module will be stopped
     */
    public void setDistance(float distance) {
	this.distance = distance;
    }

}
