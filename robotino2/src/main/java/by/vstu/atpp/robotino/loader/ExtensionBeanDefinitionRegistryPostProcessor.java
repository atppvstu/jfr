package by.vstu.atpp.robotino.loader;

import static org.springframework.util.StringUtils.uncapitalize;

import java.lang.reflect.Constructor;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.util.StringUtils;

import com.google.common.reflect.TypeToken;

import by.vstu.atpp.robotino.module.Module;
import by.vstu.atpp.robotino.service.Service;

/**
 * This class registers BeanDefinitions in the Spring ApplicationContext.
 *
 * @author isap.vstu.by
 *
 */
@Order(Ordered.LOWEST_PRECEDENCE)
public class ExtensionBeanDefinitionRegistryPostProcessor implements BeanDefinitionRegistryPostProcessor {

    private static final Log LOG = LogFactory.getLog(ExtensionBeanDefinitionRegistryPostProcessor.class);

    private Class<?>[] classes;

    private BeanFactory factory;

    /**
     * 
     * @param factory
     *            bean factory from which beans will be injected
     * @param classes
     *            classes that should be registered.
     */
    public ExtensionBeanDefinitionRegistryPostProcessor(BeanFactory factory, Class<?>[] classes) {
	this.factory = factory;
	this.classes = classes;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
	factory = beanFactory;
    }

    /**
     * Creates BeanDefinitions and register them. {@inheritDoc}
     */
    @Override
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry reg) throws BeansException {
	if (classes != null && classes.length > 0) {
	    for (Class<?> clazz : classes) {
		try {
		    BeanDefinitionBuilder builder = createBuilder(clazz);
		    Constructor<?> constructor = clazz.getConstructors()[0];
		    Class<?>[] types = constructor.getParameterTypes();
		    for (Class<?> cl : types) {
			TypeToken<?> currentToken = TypeToken.of(cl);
			if (currentToken.isSubtypeOf(Service.class)) {
			    builder.addConstructorArgValue(factory.getBean(cl));
			} else if (currentToken.isSubtypeOf(Module.class)) {
			    String name = StringUtils.uncapitalize(cl.getSimpleName());
			    if (reg.containsBeanDefinition(name)) {
				builder.addConstructorArgReference(name);
			    } else {
				builder.addConstructorArgValue(factory.getBean(cl));
			    }
			}
		    }
		    String name = uncapitalize(clazz.getSimpleName());
		    reg.registerBeanDefinition(name, builder.getBeanDefinition());

		    LOG.info("register beanDefinition: " + name);
		} catch (Throwable e) {
		    LOG.warn("can't create beanDefinition " + clazz);
		    LOG.debug(e.getStackTrace());
		    e.printStackTrace();
		}
	    }
	}
    }

    private BeanDefinitionBuilder createBuilder(Class<?> clazz) {
	return BeanDefinitionBuilder.rootBeanDefinition(clazz).setScope("prototype");
    }
}
