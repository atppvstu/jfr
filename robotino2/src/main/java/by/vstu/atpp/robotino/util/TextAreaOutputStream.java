package by.vstu.atpp.robotino.util;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

import javafx.application.Platform;
import javafx.scene.control.TextArea;

/**
 * This class is used for print text stream into the TextArea.
 *
 * @author isap.vstu.by
 *
 */
public class TextAreaOutputStream extends OutputStream {

    private TextArea output;
    private boolean err;

    private PrintStream defaultStd = System.out;
    private PrintStream defaultErr = System.err;

    /**
     * @param output
     *            TextArea to print.
     * @param err
     *            is TextArea will be used to print error messages
     *
     */
    public TextAreaOutputStream(TextArea output, boolean err) {
	this.output = output;
	this.err = err;
    }

    private final StringBuffer buffer = new StringBuffer();

    /**
     * {@inheritDoc}
     */
    @Override
    public void write(int b) throws IOException {
	buffer.append((char) b);
	flush();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void write(byte[] b) throws IOException {
	write(b, 0, b.length);
	flush();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void write(byte[] b, int off, int len) throws IOException {
	if (b == null) {
	    throw new NullPointerException();
	} else if (off < 0 || off > b.length || len < 0 || off + len > b.length || off + len < 0) {
	    throw new IndexOutOfBoundsException();
	} else if (len == 0) {
	    return;
	}

	for (int i = 0; i < len; i++) {
	    buffer.append((char) b[off + i]);
	}

	flush();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void flush() throws IOException {
	final String str;

	synchronized (buffer) {
	    str = buffer.toString();
	    buffer.delete(0, buffer.length());
	}

	Platform.runLater(new Runnable() {
	    @Override
	    public void run() {
		if (err) {
		    defaultErr.print(str);
		} else {
		    defaultStd.print(str);
		}
		output.appendText(str);
	    }
	});
    }
}
