package by.vstu.atpp.robotino.controller.panel.tab;

import java.awt.image.BufferedImage;

import org.springframework.stereotype.Component;

import by.vstu.atpp.robotino.controller.AbstractController;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * Controller that handles events of the ProcessedImage.fxml
 * 
 * @author isap.vstu.by
 *
 */
@Component
public class ProcessedImageTabController extends AbstractController {

    @FXML
    private ImageView imageView;

    @FXML
    private void initialize() {
	Image image = new Image("resources/nowifi.png");
	imageView.setImage(image);
    }

    /**
     * That method draw additional image on GUI
     * 
     * @param image
     *            image that will be drawn
     */
    public void drawImage(java.awt.Image image) {
	if (image != null) {
	    imageView.setImage(SwingFXUtils.toFXImage((BufferedImage) image, null));
	}
    }

}
