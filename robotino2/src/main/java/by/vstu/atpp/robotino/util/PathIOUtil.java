package by.vstu.atpp.robotino.util;

import static java.lang.Float.parseFloat;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import by.vstu.atpp.robotino.domain.GrapplerPosition;

/**
 * The class is used to save the list of imported modules so that you do not
 * have to import them each time.
 *
 * @author isap.vstu.by
 *
 */
public class PathIOUtil {

    /**
     * Path to a .list file with modules.
     */
    public static final String MODULES_FILE = System.getProperty("user.home")
	    + "/Documents/robotino2/modulesFolders.list";
    /**
     * Path to a .list file with positions.
     */
    public static final String POSITIONS_FILE = System.getProperty("user.home") + "/Documents/robotino2/positions.list";

    private PathIOUtil() {

    }

    /**
     * Saves list of folders with modules to a .list file.
     *
     * @param files
     *            List of modules folders to save
     */
    public static void saveFilesAsList(List<File> files) {
	saveList(files.stream().map(file -> file.getAbsolutePath()).collect(Collectors.toList()), MODULES_FILE);
    }

    /**
     * Loads list of folders with modules from a .list file.
     *
     * @return List of modules folders from .list file.
     */
    public static List<File> loadFilesAsList() {
	return loadList(MODULES_FILE).stream().map(filepath -> new File(filepath)).collect(Collectors.toList());
    }

    /**
     * Saves list of grappler positions to a .list file.
     *
     * @param positions
     *            List of grappler positions to save
     */
    public static void savePositionsAsList(Map<String, GrapplerPosition> positions) {
	saveList(positions.entrySet().stream().map(entry -> {
	    GrapplerPosition position = entry.getValue();
	    return String.format(Locale.US, "%s,%f,%f,%f,%f,%f,%f,%f,%f", entry.getKey(), position.getBottomPosition(),
		    position.getMiddlePosition(), position.getTopPosition(), position.getGrapplePosition(),
		    position.getBottomSpeed(), position.getMiddleSpeed(), position.getTopSpeed(),
		    position.getGrappleSpeed());
	}).collect(Collectors.toList()), POSITIONS_FILE);

    }

    /**
     * Loads list of grappler positions from a .list file.
     *
     * @return List of grappler positions.
     */
    public static Map<String, GrapplerPosition> loadPositionsAsList() {
	Map<String, GrapplerPosition> map = new HashMap<>();
	for (String str : loadList(POSITIONS_FILE)) {
	    String[] splitted = str.split(",");
	    String name = splitted[0];
	    GrapplerPosition position = new GrapplerPosition(parseFloat(splitted[1]), parseFloat(splitted[2]),
		    parseFloat(splitted[3]), parseFloat(splitted[4]), parseFloat(splitted[5]), parseFloat(splitted[6]),
		    parseFloat(splitted[7]), parseFloat(splitted[8]));
	    map.put(name, position);
	}
	return map;
    }

    private static void saveList(List<String> values, String path) {
	try (BufferedWriter writer = new BufferedWriter(new FileWriter(new File(path)))) {
	    for (String str : values) {
		writer.append(str);
		writer.newLine();
	    }
	} catch (IOException e) {
	    e.printStackTrace();
	}
    }

    public static List<String> loadList(String path) {
	File file = new File(path);
	List<String> values = new ArrayList<>();
	if (file.exists()) {
	    try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
		String str = null;
		while ((str = reader.readLine()) != null) {
		    values.add(str);
		}
	    } catch (IOException e) {
		e.printStackTrace();
	    }
	}
	return values;
    }

}
