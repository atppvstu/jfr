package by.vstu.atpp.robotino.module.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import by.vstu.atpp.robotino.domain.Position;
import by.vstu.atpp.robotino.domain.Speed;
import by.vstu.atpp.robotino.module.AbstractModule;
import by.vstu.atpp.robotino.service.DriveService;

/**
 * The module moves the robot to the specified position.
 * 
 * @author isap.vstu.by
 *
 */
@Component
@Scope(scopeName = "prototype")
public class GoToPositionModule extends AbstractModule {

    @Autowired
    private DriveService driveService;

    private float maxSpeed = 0.1f;
    private float xTarget = 0;
    private float yTarget = 0;

    private Position position;

    private double absPhi;
    private double resPhi;
    private double length;

    private double x;
    private double y;
    private double phi;

    private double vx;
    private double vy;

    /**
     * {@inheritDoc}
     */
    @Override
    public void perform() {
	position = driveService.getCurrentPosition();
	x = position.getX() - xTarget;
	y = position.getY() - yTarget;
	phi = position.getPhi();
	if (Math.abs(x) < 0.1f && Math.abs(y) < 0.1f) {
	    stop();
	} else {
	    length = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
	    absPhi = Math.atan(y / x);
	    resPhi = absPhi - phi;

	    vx = Math.cos(resPhi) * length;
	    vy = Math.sin(resPhi) * length;

	    if (x > 0) {
		vx = -vx;
		vy = -vy;
	    }

	    double lengthV = Math.sqrt(vx * vx + vy * vy);
	    vx /= (lengthV / maxSpeed);
	    vy /= (lengthV / maxSpeed);

	}
	driveService.setSpeed(new Speed((float) vx, (float) vy, 0));
    }

    /**
     * @param maxSpeed
     *            robot maximum speed
     */
    public void setMaxSpeed(float maxSpeed) {
	this.maxSpeed = maxSpeed;
    }

    /**
     * x-position in which the robot must go
     * 
     * @param xTarget
     *            position in meters
     */
    public void setxTarget(float xTarget) {
	this.xTarget = xTarget;
    }

    /**
     * y-position in which the robot must go
     * 
     * @param yTarget
     *            position in meters
     */
    public void setyTarget(float yTarget) {
	this.yTarget = yTarget;
    }

}
