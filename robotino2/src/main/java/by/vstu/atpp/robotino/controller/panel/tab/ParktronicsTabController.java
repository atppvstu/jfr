package by.vstu.atpp.robotino.controller.panel.tab;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import by.vstu.atpp.robotino.controller.AbstractController;
import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;

/**
 * Controller that handles events of the ParktronicsTab.fxml
 * 
 * @author isap.vstu.by
 *
 */
@Component
public class ParktronicsTabController extends AbstractController {

    /**
     * Maximum values of the parktronics distance while robot is not connected
     */
    public static final double[] MAX_VALUES = { 0.41, 0.41, 0.41, 0.41, 0.41, 0.41, 0.41, 0.41, 0.41 };

    @FXML
    private Group group;

    private List<Line> lines;

    @FXML
    private void initialize() {
	lines = new ArrayList<>();
	Line line = null;
	for (int i = 0; i <= 8; i++) {
	    line = new Line();
	    line.setStrokeWidth(2.0);
	    lines.add(line);
	}
	group.getChildren().addAll(lines);
	setDistance(MAX_VALUES);
    }

    /**
     * Show distance on the ParktronicsTab
     * 
     * @param doubles
     *            distance values that will be shown
     */
    public void setDistance(double[] doubles) {
	Line line = null;
	for (int i = 0; i < doubles.length; i++) {
	    line = lines.get(i);
	    double distPercent = doubles[i] / 0.41;
	    line.setEndY((-distPercent * 75 - 25) * Math.cos(Math.toRadians(-i * 40)));
	    line.setEndX((distPercent * 75 + 25) * Math.sin(Math.toRadians(-i * 40)));
	    line.setStroke(Color.rgb((int) (255 - distPercent * 255), 0, 0));
	}
    }

}
