package by.vstu.atpp.robotino.action.impl;

import by.vstu.atpp.robotino.action.AbstractAction;
import by.vstu.atpp.robotino.action.ActionClass;
import by.vstu.atpp.robotino.module.impl.GoToPositionModule;
import by.vstu.atpp.robotino.module.impl.TurnToAngleModule;

/**
 * This action consists of the following modules:
 * <ul>
 * <li>{@link TurnToAngleModule TurnToAngleModule}</li>
 * <li>{@link GoToPositionModule GoToPositionModule}</li>
 * </ul>
 * 
 * @author isap.vstu.by
 *
 */
@ActionClass
public class GoToZeroAction extends AbstractAction {

    /**
     * 
     * @param turnToAngleModule
     *            The module to perform.
     * @param goToPositionModule
     *            The module to perform.
     */
    public GoToZeroAction(TurnToAngleModule turnToAngleModule, GoToPositionModule goToPositionModule) {
	goToPositionModule.setMaxSpeed(0.3f);
	goToPositionModule.setxTarget(0);
	goToPositionModule.setyTarget(0);
	turnToAngleModule.setPhiTarget(0);
	addLast(goToPositionModule);
	addLast(turnToAngleModule);
    }

}
