package by.vstu.atpp.robotino.view;

import java.io.IOException;
import java.io.InputStream;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import by.vstu.atpp.robotino.controller.Controller;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.util.Callback;

/**
 * This class is used for load FXML GUI from the files and create
 * {@link Controller Controllers}
 * 
 * @author isap.vstu.by
 *
 */
@Component
public class FXMLHelper implements ApplicationContextAware {

    private ApplicationContext ctx;

    /**
     * Loads FXML GUI from the file.
     * 
     * @param url
     *            Path to the FXML
     * @return Controller of the current FXML file.
     */
    public Controller load(String url) {
	try (InputStream fxmlStream = MainFrame.class.getResourceAsStream(url)) {
	    FXMLLoader loader = new FXMLLoader();
	    loader.setControllerFactory(new Callback<Class<?>, Object>() {
		@Override
		public Object call(Class<?> aClass) {
		    return ctx.getBean(aClass);
		}
	    });
	    loader.setLocation(MainFrame.class.getResource(url));
	    Node view = (Node) loader.load(fxmlStream);
	    Controller controller = loader.getController();
	    controller.setView(view);

	    return controller;
	} catch (IOException e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setApplicationContext(ApplicationContext ctx) throws BeansException {
	this.ctx = ctx;
    }

}
