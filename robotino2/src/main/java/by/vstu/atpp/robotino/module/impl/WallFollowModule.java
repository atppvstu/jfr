package by.vstu.atpp.robotino.module.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import by.vstu.atpp.robotino.action.impl.WallFollowAction;
import by.vstu.atpp.robotino.domain.Speed;
import by.vstu.atpp.robotino.module.AbstractModule;
import by.vstu.atpp.robotino.service.SensorService;
import by.vstu.atpp.robotino.service.DriveService;

/**
 * This Module moves the robot along the wall without rotating.
 * 
 * @author isap.vstu.by
 *
 * @see WallFollowAction
 */
@Component
public class WallFollowModule extends AbstractModule {

    @Autowired
    private DriveService driveService;
    @Autowired
    private SensorService inputService;

    private float distance = 0.1f;
    private float defaultSpeed = 0.2f;
    private boolean clockwiseDirection = true;

    private int min;
    private float vx, vy;

    /**
     * {@inheritDoc}
     */
    @Override
    public void perform() {
	min = 0;
	for (int i = 0; i < 9; i++) {
	    if (getDistance(i) < getDistance(min)) {
		min = i;
	    }
	}
	int minl = (min + 1) % 9;
	int minp = (min + 8) % 9;

	float dist = getDistance(min);

	float distl = getDistance(minl) + 0.225f;
	float distp = getDistance(minp) + 0.225f;

	if (dist > distance && dist < distance + .1f) {
	    double deg = Math.toDegrees(Math.atan((distl - distp) * Math.cos(Math.toRadians(40)))
		    / ((distl + distp) * Math.sin(Math.toRadians(40))));
	    degVelocity(min * 40 + 90 - deg, clockwiseDirection);
	} else {
	    correct(dist);
	}
	driveService.setSpeed(new Speed(vx, vy, 0));
    }

    private void degVelocity(double degree, boolean forward) {
	degree = Math.toRadians(degree);
	vx = (float) (defaultSpeed * Math.cos(degree));
	vy = (float) (defaultSpeed * Math.sin(degree));
	if (!forward) {
	    vx = -vx;
	    vy = -vy;
	}
    }

    private void correct(double dist) {
	if (dist > distance + 0.15f) {
	    degVelocity(min * 40, true);
	}
	if (dist < distance + 0.05f) {
	    degVelocity(min * 40, false);
	}
    }

    private float getDistance(int num) {
	return inputService.getSensors().get(num).distance();
    }

    /**
     * Sets the distance from which the robot will hold the wall.
     * 
     * @param distance
     *            distance in meters.
     */
    public void setDistance(float distance) {
	this.distance = distance;
    }

    /**
     * The speed with which the robot moves along the wall
     * 
     * @param defaultSpeed
     *            speed in meters per second
     */
    public void setDefaultSpeed(float defaultSpeed) {
	this.defaultSpeed = defaultSpeed;
    }

    /**
     * Direction of movement.
     * 
     * @param clockwiseDirection
     *            Direction. Robot moves Right if true.
     */
    public void setClockwiseDirection(boolean clockwiseDirection) {
	this.clockwiseDirection = clockwiseDirection;
    }

}
