package by.vstu.atpp.robotino.loader;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.util.ReflectionUtils;

import com.google.common.reflect.TypeToken;

import by.vstu.atpp.robotino.action.Action;
import by.vstu.atpp.robotino.module.Module;
import by.vstu.atpp.robotino.service.Service;

/**
 * 
 * Spring BeanPostProcessor for Action and Module classes
 * 
 * @author isap.vstu.by
 *
 */
public class ExtensionBeanPostProcessor implements BeanPostProcessor {

    private static final Log LOG = LogFactory.getLog(ExtensionBeanPostProcessor.class);

    @Autowired
    private BeanFactory ctx;

    /**
     * Injects fileds into Module and Action beans. {@inheritDoc}
     */
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
	TypeToken<?> currentToken = TypeToken.of(bean.getClass());

	if (currentToken.isSubtypeOf(Module.class)) {
	    doWithFields(bean, Service.class);
	} else if (currentToken.isSubtypeOf(Action.class)) {
	    doWithFields(bean, Module.class);
	    doWithFields(bean, Service.class);
	}
	return bean;
    }

    private void doWithFields(Object bean, Class<?> token) {
	ReflectionUtils.doWithFields(bean.getClass(), field -> {
	    Object fieldValue;
	    try {
		fieldValue = ctx.getBean(field.getType());
	    } catch (BeansException e) {
		// TODO: check it out later
		LOG.info("can't find bean " + field.getType());
		return;
	    }
	    ReflectionUtils.makeAccessible(field);
	    ReflectionUtils.setField(field, bean, fieldValue);
	}, field -> {
	    return TypeToken.of(field.getType()).isSubtypeOf(token) && field.getAnnotation(Inject.class) != null;
	});
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
	return bean;
    }
}
