package by.vstu.atpp.robotino.service.impl;

import org.springframework.stereotype.Service;

import by.vstu.atpp.robotino.service.RobotService;
import by.vstu.atpp.robotino.service.ShutdownService;

/**
 * This class is default implementation of the {@link ShutdownService
 * ShutdownService}.
 *
 * @author isap.vstu.by
 *
 */
@Service
public class ShutdownServiceImpl extends RobotService implements ShutdownService {

    /**
     * {@inheritDoc}
     */
    @Override
    public void shutdown() {
	robot.shutdown();
    }

}
