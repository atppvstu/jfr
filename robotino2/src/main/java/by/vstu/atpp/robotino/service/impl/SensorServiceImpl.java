package by.vstu.atpp.robotino.service.impl;

import static by.vstu.atpp.robotino.domain.BatteryStatus.OFFLINE;
import static by.vstu.atpp.robotino.view.ViewUtil.loadImage;

import java.awt.Image;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;

import by.vstu.atpp.robotino.domain.BatteryStatus;
import by.vstu.atpp.robotino.service.RobotService;
import by.vstu.atpp.robotino.service.SensorService;
import rec.robotino.api2.DistanceSensor;

/**
 * This class is default implementation of the {@link SensorService
 * DataInputService}.
 *
 * @author isap.vstu.by
 *
 */
@Service
public class SensorServiceImpl extends RobotService implements SensorService {

    private volatile Image image;
    private volatile BatteryStatus batteryStatus;

    private List<DistanceSensor> sensors;

    @PostConstruct
    public void init() {
	sensors = robot.getDistanceSensors();

	initStubs();

	robot.addBatteryListener((current, isCharging) -> batteryStatus = new BatteryStatus(current, isCharging));
	robot.addImageListener(image -> this.image = image);
    }

    private void initStubs() {
	image = loadImage("/resources/nowifi.png");
	batteryStatus = OFFLINE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Image getImage() {
	return image;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BatteryStatus getBatteryStatus() {
	return batteryStatus;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<DistanceSensor> getSensors() {
	return sensors;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isBumpered() {
	return robot.isBumper();
    }

}
