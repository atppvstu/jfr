package by.vstu.atpp.robotino.module.impl;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import by.vstu.atpp.robotino.domain.Speed;
import by.vstu.atpp.robotino.module.AbstractModule;
import by.vstu.atpp.robotino.service.DriveService;
import by.vstu.atpp.robotino.service.IOService;
import rec.robotino.api2.DigitalInput;

/**
 * The module moves the robot through the black line
 * 
 * @author isap.vstu.by
 *
 */
@Component
@Scope(scopeName = "prototype")
public class LineFollowModule extends AbstractModule {

    @Autowired
    private IOService ioService;
    @Autowired
    private DriveService driveService;

    private float moveSpeed = 0.1f;
    private float rotateSpeed = 0.3f;
    private boolean outside = false;

    private DigitalInput leftOptical;
    private DigitalInput rightOptical;

    private float omega;
    private boolean blackLine;

    @PostConstruct
    private void init() {
	leftOptical = ioService.getDigitalInputs().get(0);
	rightOptical = ioService.getDigitalInputs().get(1);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void perform() {
	if (outside) {
	    rotateSpeed = -rotateSpeed;
	}
	boolean left = leftOptical.value();
	boolean right = rightOptical.value();
	if (blackLine) {
	    left = !left;
	    right = !right;
	}
	if (left) {
	    omega = rotateSpeed;
	} else if (right) {
	    omega = -rotateSpeed;
	} else {
	    omega = 0;
	}
	driveService.setSpeed(new Speed(moveSpeed, 0, omega));
    }

    /**
     * The location of the sensors relative to the line.
     * 
     * @param outside
     *            True if the sensors are above the line. False if the sensors on
     *            the left and right of it
     */
    public void setOutside(boolean outside) {
	this.outside = outside;
    }

    /**
     * @param rotateSpeed
     *            speed of rotation
     */
    public void setRotateSpeed(float rotateSpeed) {
	this.rotateSpeed = rotateSpeed;
    }

    /**
     * @param blackLine
     *            If line is black on white background set true.
     */
    public void setBlackLine(boolean blackLine) {
	this.blackLine = blackLine;
    }

}
