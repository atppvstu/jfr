package by.vstu.atpp.robotino.util;

import by.vstu.atpp.robotino.domain.Speed;
import by.vstu.atpp.robotino.util.validator.DefaultSpeedValidator;
import by.vstu.atpp.robotino.util.validator.Validator;

/**
 * Util class to work with {@link Speed Speed}.
 * 
 * @author isap.vstu.by
 *
 */
public final class Speeds {

    private static Validator<Speed> validator = new DefaultSpeedValidator();

    public static final Speed ZERO_SPEED = new Speed(0F, 0F, 0F);

    private Speeds() {

    }

    /**
     * Get sum of the speeds.
     * 
     * @param speed
     *            speed to sum
     * @param speed2
     *            speed to sum
     * @return speed + speed2
     */
    public static Speed add(Speed speed, Speed speed2) {
	float vx = speed.getVx() + speed2.getVx();
	float vy = speed.getVy() + speed2.getVy();
	float omega = speed.getOmega() + speed2.getOmega();

	Speed newSpeed = new Speed(vx, vy, omega);

	return validate(newSpeed);
    }

    /**
     * Validate the speed
     * 
     * @param speed
     *            speed to validate.
     * @return valid speed.
     */
    public static Speed validate(Speed speed) {
	return validator.validate(speed);
    }
}
