package by.vstu.atpp.robotino.module.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import by.vstu.atpp.robotino.domain.Speed;
import by.vstu.atpp.robotino.module.AbstractModule;
import by.vstu.atpp.robotino.service.SensorService;
import by.vstu.atpp.robotino.service.DriveService;

/*
 * This module rotates the robot sensor to the object
 */
@Component
@Scope("prototype")
public class TurnToSensorModule extends AbstractModule {

    @Autowired
    private SensorService inputService;
    @Autowired
    private DriveService driveService;

    private int sensorNumber = 0;
    private float rotateSpeed = 0.3f;

    private int nearestSensor;
    private float omega;

    /**
     * {@inheritDoc}
     */
    @Override
    public void perform() {
	for (int i = 0; i < 9; i++) {
	    if (getDistance(i) < getDistance(nearestSensor)) {
		nearestSensor = i;
	    }
	}
	if (nearestSensor != sensorNumber) {
	    if ((sensorNumber - nearestSensor + 9) % 9 >= 5) {
		omega = rotateSpeed;
	    } else {
		omega = -rotateSpeed;
	    }
	    driveService.setSpeed(new Speed(0, 0, omega));
	} else {
	    stop();
	}
    }

    private float getDistance(int num) {
	return inputService.getSensors().get(num).distance();
    }

    /**
     * @param sensorNumber
     *            Sensor to turn
     */
    public void setSensorNumber(int sensorNumber) {
	this.sensorNumber = sensorNumber;
    }

    /**
     * @param rotateSpeed
     *            speed of rotate
     */
    public void setRotateSpeed(float rotateSpeed) {
	this.rotateSpeed = rotateSpeed;
    }

}
