package by.vstu.atpp.robotino.robot.listener;

public interface BatteryListener {

    /**
     * Invokes when odometry data will be received from the robot.
     * 
     * @param current
     *            battery voltage
     * @param isCharging
     *            true if battery is charging
     */
    void onBatteryReceived(float current, boolean isCharging);
}
