package by.vstu.atpp.robotino.controller.panel.tab;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import by.vstu.atpp.robotino.controller.AbstractController;
import by.vstu.atpp.robotino.service.ExtensionImportService;
import by.vstu.atpp.robotino.util.PathIOUtil;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.stage.DirectoryChooser;

/**
 * Controller that handles events of the ManagerFrame.fxml
 *
 * @author isap.vstu.by
 *
 */
@Component
public class ManagerTabController extends AbstractController {

    @FXML
    private ListView<String> fileList;
    @FXML
    private ListView<String> moduleList;
    @FXML
    private ListView<String> actionsList;
    @FXML
    private ObservableList<File> files;
    @FXML
    private ObservableList<File> modules;
    @FXML
    private ObservableList<File> actions;

    private List<File> folders = new ArrayList<>();

    @Autowired
    private ExtensionImportService importService;

    @FXML
    private void initialize() {
	files.addAll(PathIOUtil.loadFilesAsList());
	update();
	apply();
    }

    @PreDestroy
    private void close() {
	PathIOUtil.saveFilesAsList(files);
    }

    @FXML
    private void addFolder() {
	DirectoryChooser chooser = new DirectoryChooser();
	chooser.setTitle("Select Extensions Folder...");
	File dir = chooser.showDialog(fileList.getScene().getWindow());
	if (dir != null) {
	    files.add(dir);
	}

    }

    @FXML
    private void removeFolder() {
	int index = fileList.getSelectionModel().getSelectedIndex();
	if (index != -1) {
	    files.remove(index);
	}
    }

    @FXML
    private void update() {
	modules.clear();
	actions.clear();
	folders.clear();
	for (File dir : files) {
	    scanFolder(dir);
	}
    }

    private void scanFolder(File folder) {
	for (File file : folder.listFiles()) {
	    if (file.isDirectory()) {
		folders.add(file);
		scanFolder(file);
	    } else if (file.getName().endsWith(".r3m")) {
		modules.add(file);
	    } else if (file.getName().endsWith(".r3a")) {
		actions.add(file);
	    }
	}
    }

    @FXML
    private void apply() {
	if (!folders.isEmpty()) {
	    importService.importFrom(folders);
	}
    }

}
