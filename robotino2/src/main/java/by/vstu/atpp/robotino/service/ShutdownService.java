package by.vstu.atpp.robotino.service;

/**
 * That class is used to shutdown the robot
 * 
 * @author isap.vstu.by
 *
 */
public interface ShutdownService extends Service {

    /**
     * Shutdowns the robot
     */
    void shutdown();

}
