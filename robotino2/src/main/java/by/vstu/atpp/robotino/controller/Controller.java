package by.vstu.atpp.robotino.controller;

import javafx.scene.Node;

/**
 * Base class for any controller that used for bean injection by SpringFramework
 * 
 * @author isap.vstu.by
 *
 */
public interface Controller {

    /**
     * @return JavaFX Node stored in this controller
     */
    Node getView();

    /**
     * @param view
     *            Node that will be stored in this controller
     * 
     */
    void setView(Node view);

}
