package by.vstu.atpp.robotino.loader;

import javax.annotation.PostConstruct;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

/**
 * This class stores RefreshableApplicationContext is used for Actions and
 * Modules
 * 
 * @author isap.vstu.by
 *
 */
@Component
public class RefreshableApplicationContextHolder {

    private static final Log LOG = LogFactory.getLog(RefreshableApplicationContextHolder.class);

    @Autowired
    private ApplicationContext root;

    private ClassPathXmlApplicationContext child;

    @PostConstruct
    private void init() {
	child = new ClassPathXmlApplicationContext(new String[] { "config/spring-rf-config.xml" }, root);
    }

    /**
     * Refresh stored RefreshableApplicationContext
     */
    public void refresh() {
	child.refresh();
	LOG.info("context refreshed");
    }

    /**
     * @return stored RefreshableApplicationContext
     */
    public ClassPathXmlApplicationContext getContext() {
	return child;
    }

}
