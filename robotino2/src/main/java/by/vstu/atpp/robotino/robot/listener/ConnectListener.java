package by.vstu.atpp.robotino.robot.listener;

public interface ConnectListener {

    /**
     * Invokes after connection with robot will be created.
     */
    void onConnected();

    /**
     * Invokes after connection with robot will be closed.
     */
    void onDisconnected();

    /**
     * Invokes when connection error will be detected.
     * 
     * @param error
     *            error information.
     */
    void onError(String error);

}
