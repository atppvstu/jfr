package by.vstu.atpp.robotino.controller.panel.tab;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import by.vstu.atpp.robotino.controller.AbstractController;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;

/**
 * Controller that handles events of the InputsTab.fxml
 * 
 * @author isap.vstu.by
 *
 */
@Component
public class InputsTabController extends AbstractController {

    /**
     * Zero values of the analog ports that shown while robot is not connected
     */
    public static final double[] ZERO_VALUES = { 0, 0, 0, 0, 0, 0, 0, 0 };
    /**
     * False values of the digital ports that shown while robot is not connected
     */
    public static final boolean[] FALSE_VALUES = { false, false, false, false, false, false, false, false };

    @FXML
    private GridPane grid;

    private List<Label> analogValues;
    private List<Label> digitalValues;

    @FXML
    private void initialize() {
	analogValues = new ArrayList<>();
	digitalValues = new ArrayList<>();
	Label label = null;
	for (int i = 1; i <= 8; i++) {
	    label = new Label();
	    label.setText("DI" + i + ": ");
	    grid.add(label, 0, i);
	}
	for (int i = 1; i <= 8; i++) {
	    label = new Label("false");
	    digitalValues.add(label);
	    grid.add(label, 1, i);
	}
	for (int i = 1; i <= 8; i++) {
	    label = new Label();
	    label.setText("AI" + i + ": ");
	    grid.add(label, 2, i);
	}
	for (int i = 1; i <= 8; i++) {
	    label = new Label("0.0");
	    analogValues.add(label);
	    grid.add(label, 3, i);
	}
    }

    /**
     * Show value of the digital inputs on the InputTab
     * 
     * @param digitalInputs
     *            digitalInputs values that will be shown
     */
    public void setDigitalValues(boolean[] digitalInputs) {
	Platform.runLater(() -> {
	    Label label;
	    for (int i = 0; i < digitalInputs.length; i++) {
		label = digitalValues.get(i);
		label.setText(String.valueOf(digitalInputs[i]));
	    }
	});
    }

    /**
     * Show value of the analog inputs on the InputTab
     * 
     * @param analogInputs
     *            analogInputs values that will be shown
     */
    public void setAnalogValues(double[] analogInputs) {
	Platform.runLater(() -> {
	    Label label;
	    for (int i = 0; i < analogInputs.length; i++) {
		label = analogValues.get(i);
		label.setText(String.format("%.2f", analogInputs[i]));
	    }
	});
    }

}
