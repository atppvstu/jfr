package by.vstu.atpp.robotino.view;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ViewUtil {

    private ViewUtil() {

    }

    public static BufferedImage loadImage(String filename) {
	try {
	    return ImageIO.read(ViewUtil.class.getResource(filename));
	} catch (IOException e) {
	    e.printStackTrace();
	    return null;
	}
    }

}
