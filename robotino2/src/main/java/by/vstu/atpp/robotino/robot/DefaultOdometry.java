package by.vstu.atpp.robotino.robot;

import java.util.ArrayList;
import java.util.List;

import by.vstu.atpp.robotino.robot.listener.PositionListener;
import rec.robotino.api2.Odometry;

/**
 * This class extends Grappler class from robotino api2
 * 
 * @author isap.vstu.by
 *
 */
public class DefaultOdometry extends Odometry {

    private List<PositionListener> listeners = new ArrayList<>();

    /**
     * Called when position and robot speed was received
     */
    @Override
    public void readingsEvent(double x, double y, double phi, float vx, float vy, float omega, long sequence) {
	for (PositionListener listener : listeners) {
	    listener.onOdometryReceived(x, y, phi, vx, vy, omega);
	}
    }

    /**
     * Add listener of grappler position
     * 
     * @param listener
     *            listener to add
     */
    public void addListener(PositionListener listener) {
	listeners.add(listener);
    }

    /**
     * Remove listener of grappler position
     * 
     * @param listener
     *            listener to remove
     */
    public void removeListener(PositionListener listener) {
	listeners.remove(listener);
    }
}