package by.vstu.atpp.robotino.service;

import java.awt.Color;
import java.awt.Image;

/**
 * This class is used to interact with services from GUI
 * 
 * @author isap.vstu.by
 *
 */
public interface UpdatableGuiService extends GuiService {

    /**
     * Sets speed which will be returned as a speed set on the slider
     * 
     * @param sliderSpeed
     *            speed to set
     */
    void setSliderSpeed(float sliderSpeed);

    /**
     * Sets rangeColor which will be returned as a range selected by user on image
     * 
     * @param rangeColor
     *            range to set
     */
    void setRangeColor(Color[] rangeColor);

    /**
     * Sets color which will be returned as a color selected by user on image
     * 
     * @param singleColor
     *            color to set
     */
    void setSingleColor(Color singleColor);

    /**
     * @return image which was set as a processed image
     */
    Image getProcessedImage();

}
