package by.vstu.atpp.robotino.service;

import java.io.File;
import java.util.List;

/**
 * This class is used to import external modules in the application.
 * 
 * @author isap.vstu.by
 *
 */
public interface ExtensionImportService extends Service {

    /**
     * Import Modules and Actions from folders.
     * 
     * @param folders
     *            where Actions and Modules will be searched
     */
    void importFrom(List<File> folders);
}
