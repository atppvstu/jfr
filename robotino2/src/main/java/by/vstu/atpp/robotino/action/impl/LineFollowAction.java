package by.vstu.atpp.robotino.action.impl;

import org.springframework.beans.factory.annotation.Autowired;

import by.vstu.atpp.robotino.action.AbstractAction;
import by.vstu.atpp.robotino.action.ActionClass;
import by.vstu.atpp.robotino.module.impl.AroundObjectModule;
import by.vstu.atpp.robotino.module.impl.LineFollowModule;
import by.vstu.atpp.robotino.module.impl.TurnToSensorModule;
import by.vstu.atpp.robotino.service.IOService;
import by.vstu.atpp.robotino.service.SensorService;

/**
 * This action consists of the following modules:
 * <ul>
 * <li>{@link LineFollowModule LineFollowModule} with stop condition by
 * distance</li>
 * <li>{@link TurnToSensorModule TurnToSensorModule}</li>
 * <li>{@link AroundObjectModule AroundObjectModule} with stop condition by line
 * detect</li>
 * </ul>
 * 
 * The robot moves along the line, circling obstacles encountered along the way.
 * 
 * @author isap.vstu.by
 *
 */
@ActionClass
public class LineFollowAction extends AbstractAction {

    @Autowired
    private SensorService sensorService;
    @Autowired
    private IOService ioService;

    /**
     * @param lineFollowModule
     *            The module to perform.
     * @param turnToSensorModule
     *            The module to perform.
     * @param aroundObjectModule
     *            The module to perform.
     */
    @Autowired
    public LineFollowAction(LineFollowModule lineFollowModule, TurnToSensorModule turnToSensorModule,
	    AroundObjectModule aroundObjectModule) {
	lineFollowModule.setStopCondition(() -> {
	    return sensorService.getSensors().get(0).distance() < 0.1f;
	});
	turnToSensorModule.setSensorNumber(7);
	aroundObjectModule.setStopCondition(() -> {
	    return !(ioService.getDigitalInputs().get(0).value() || ioService.getDigitalInputs().get(0).value());
	});
	addLast(lineFollowModule);
	addLast(turnToSensorModule);
	addLast(aroundObjectModule);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void afterLast() {
	setModuleNumber(0);
    }

}
