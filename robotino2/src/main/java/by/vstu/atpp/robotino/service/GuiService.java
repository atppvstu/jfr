package by.vstu.atpp.robotino.service;

import java.awt.Color;
import java.awt.Image;

/**
 * This class is used for interaction with GUI.
 * 
 * @author isap.vsty.by
 *
 */
public interface GuiService extends Service {

    /**
     * @return Color range selected by user on the CameraWidget.
     */
    Color[] getRangeColor();

    /**
     * 
     * @return Color selected by user while Ctrl was pressed.
     */
    Color getSingleColor();

    /**
     * @return value of speed set by user on slider
     */
    float getSliderSpeed();

    /**
     * Sets image to the ProcessedImage tab
     * 
     * @param image
     *            image to set
     */
    void setProcessedImage(Image image);

}
