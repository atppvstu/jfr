package by.vstu.atpp.robotino.action.impl;

import org.springframework.beans.factory.annotation.Autowired;

import by.vstu.atpp.robotino.action.AbstractAction;
import by.vstu.atpp.robotino.action.ActionClass;
import by.vstu.atpp.robotino.module.impl.AroundObjectModule;
import by.vstu.atpp.robotino.module.impl.TurnToSensorModule;

/**
 * This action consists of the following modules:
 * <ul>
 * <li>{@link TurnToSensorModule TurnToSensorModule}</li>
 * <li>{@link AroundObjectModule AroundObjectModule}</li>
 * </ul>
 * 
 * @author isap.vstu.by
 *
 */
@ActionClass
public class AroundObjectAction extends AbstractAction {

    /**
     * @param turnToSensorModule
     *            The module to perform.
     * @param aroundObjectModule
     *            The module to perform.
     */
    @Autowired
    public AroundObjectAction(TurnToSensorModule turnToSensorModule, AroundObjectModule aroundObjectModule) {
	turnToSensorModule.setSensorNumber(7);
	addLast(turnToSensorModule);
	addLast(aroundObjectModule);
    }

}
