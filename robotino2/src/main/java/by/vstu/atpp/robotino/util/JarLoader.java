package by.vstu.atpp.robotino.util;

import java.io.File;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;

/**
 * That class is used for load external libraries that needed for modules work
 * 
 * @author isap.vstu.by
 *
 */
public class JarLoader {

    // TODO add to config
    public static final String ROBOTINO2_EXT_LIB = System.getProperty("user.home") + "/Documents/robotino2/extLib";

    private JarLoader() {
    }

    /**
     * Add libraries from "extLib" folder to the java build path.
     */
    public static void updateLibraries() {
	File directory = new File(ROBOTINO2_EXT_LIB);
	if (!directory.exists()) {
	    directory.mkdirs();
	    return;
	}
	File[] jarnames = directory.listFiles((dir, name) -> {
	    return name.endsWith(".jar");
	});
	if (jarnames != null) {
	    for (File jar : jarnames) {
		addJar(jar.getAbsolutePath());
	    }
	}
    }

    private static void addJar(String path) {
	try {
	    URLClassLoader loader = (URLClassLoader) JarLoader.class.getClassLoader();
	    Method method = URLClassLoader.class.getDeclaredMethod("addURL", URL.class);
	    method.setAccessible(true);
	    method.invoke(loader, new URL("file:" + path));
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

}
