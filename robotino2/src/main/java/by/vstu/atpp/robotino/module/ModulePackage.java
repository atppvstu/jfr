package by.vstu.atpp.robotino.module;

import java.io.Serializable;

/**
 * That class stores Action or Module name and it's byte code.
 * 
 * @author isap.vstu.by
 *
 */
@SuppressWarnings("serial")
public class ModulePackage implements Serializable {

    private String moduleClass;

    private byte[] classByteArray;

    /**
     * 
     * @param moduleClass
     *            class name.
     * @param classByteArray
     *            class byte code.
     */
    public ModulePackage(String moduleClass, byte[] classByteArray) {
	this.moduleClass = moduleClass;
	this.classByteArray = classByteArray;
    }

    /**
     * @return class name.
     */
    public String getModuleClass() {
	return moduleClass;
    }

    /**
     * @return class byte code.
     */
    public byte[] getClassByteArray() {
	return classByteArray;
    }

}
