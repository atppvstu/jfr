package by.vstu.atpp.robotino.robot;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import by.vstu.atpp.robotino.robot.listener.BatteryListener;
import by.vstu.atpp.robotino.robot.listener.ConnectListener;
import by.vstu.atpp.robotino.robot.listener.ImageListener;
import by.vstu.atpp.robotino.robot.listener.PositionListener;
import by.vstu.atpp.robotino.robot.listener.StorePositionListener;
import rec.robotino.api2.AnalogInput;
import rec.robotino.api2.Bumper;
import rec.robotino.api2.ComId;
import rec.robotino.api2.DigitalInput;
import rec.robotino.api2.DigitalOutput;
import rec.robotino.api2.DistanceSensor;
import rec.robotino.api2.Grappler;
import rec.robotino.api2.Motor;
import rec.robotino.api2.Odometry;
import rec.robotino.api2.OmniDrive;
import rec.robotino.api2.Relay;
import rec.robotino.api2.Shutdown;

/**
 * This class is responsible for the interaction a physical robot with the
 * application. The {@link Robot Robot} stores classes of all mechanisms of the
 * robot.
 * 
 * @author isap.vstu.by
 *
 */
@Component
public class Robot {

    private DefaultCom com = new DefaultCom();
    private final Motor motor1 = new Motor();
    private final Motor motor2 = new Motor();
    private final Motor motor3 = new Motor();
    private final OmniDrive omniDrive = new OmniDrive();
    private final Bumper bumper = new Bumper();
    private final DefaultCamera camera = new DefaultCamera();
    private final DefaultOdometry odometry = new DefaultOdometry();
    private final Shutdown shutdown = new Shutdown();
    private final DefaultPowerManagement powerManagement = new DefaultPowerManagement();
    private final DefaultGrappler grappler = new DefaultGrappler();

    private final List<DistanceSensor> distanceSensors = new ArrayList<>();
    private final List<AnalogInput> analogInputs = new ArrayList<>();
    private final List<DigitalInput> digitalInputs = new ArrayList<>();
    private final List<DigitalOutput> digitalOutputs = new ArrayList<>();
    private final List<Relay> relays = new ArrayList<>();

    public Robot() {
	for (int i = 0; i < 9; ++i) {
	    DistanceSensor ds = new DistanceSensor();
	    ds.setSensorNumber(i);
	    distanceSensors.add(ds);
	}
	for (int i = 0; i < 8; i++) {
	    AnalogInput input = new AnalogInput();
	    input.setInputNumber(i);
	    analogInputs.add(input);
	}
	for (int i = 0; i < 8; i++) {
	    DigitalInput input = new DigitalInput();
	    input.setInputNumber(i);
	    digitalInputs.add(input);
	}
	for (int i = 0; i < 8; i++) {
	    DigitalOutput output = new DigitalOutput();
	    output.setOutputNumber(i);
	    digitalOutputs.add(output);
	}
	for (int i = 0; i < 2; i++) {
	    Relay relay = new Relay();
	    relay.setRelayNumber(i);
	    relays.add(relay);
	}
    }

    void init() {

	ComId id = com.id();

	motor1.setComId(id);
	motor1.setMotorNumber(0);

	motor2.setComId(id);
	motor2.setMotorNumber(1);

	motor3.setComId(id);
	motor3.setMotorNumber(2);

	omniDrive.setComId(id);
	bumper.setComId(id);
	camera.setComId(id);
	odometry.setComId(id);

	shutdown.setComId(id);
	powerManagement.setComId(id);

	grappler.setComId(id);

	for (int i = 0; i < 9; ++i) {
	    DistanceSensor ds = new DistanceSensor();
	    ds.setSensorNumber(i);
	    ds.setComId(id);
	    distanceSensors.add(ds);
	}

	for (int i = 0; i < 8; i++) {
	    AnalogInput input = new AnalogInput();
	    input.setInputNumber(i);
	    input.setComId(id);
	    analogInputs.add(input);
	}

	for (int i = 0; i < 8; i++) {
	    DigitalInput input = new DigitalInput();
	    input.setInputNumber(i);
	    input.setComId(id);
	    digitalInputs.add(input);
	}

	for (int i = 0; i < 8; i++) {
	    DigitalOutput output = new DigitalOutput();
	    output.setOutputNumber(i);
	    output.setComId(id);
	    digitalOutputs.add(output);
	}

	for (int i = 0; i < 2; i++) {
	    Relay relay = new Relay();
	    relay.setRelayNumber(0);
	    relay.setComId(id);
	    relays.add(relay);
	}
    }

    /**
     * Add StorePositionListener to the robot.
     * 
     * @param listener
     *            listener that has to be added.
     */
    public void addStorePositionListener(StorePositionListener listener) {
	grappler.addListener(listener);
    }

    /**
     * Remove StorePositionListener from the robot.
     * 
     * @param listener
     *            listener to delete.
     */
    public void removeStorePositionListener(StorePositionListener listener) {
	grappler.removeListener(listener);

    }

    /**
     * Add ConnectListener to the robot.
     * 
     * @param listener
     *            listener to be add.
     */
    public void addConnectListener(ConnectListener listener) {
	com.addListener(listener);
    }

    /**
     * Remove ConnectListener from the robot.
     * 
     * @param listener
     *            listener to delete.
     */
    public void removeConnectListener(ConnectListener listener) {
	com.removeListener(listener);
    }

    /**
     * Add ImageListener to the robot.
     * 
     * @param listener
     *            listener to be add.
     */
    public void addImageListener(ImageListener listener) {
	camera.addListener(listener);
    }

    /**
     * Remove ImageListener from the robot.
     * 
     * @param listener
     *            listener to delete.
     */
    public void removeImageListener(ImageListener listener) {
	camera.removeListener(listener);
    }

    /**
     * Add PositionListener to the robot.
     * 
     * @param listener
     *            listener to be add.
     */
    public void addPositionListener(PositionListener listener) {
	odometry.addListener(listener);
    }

    /**
     * Remove PositionListener from the robot.
     * 
     * @param listener
     *            listener to delete.
     */
    public void removePositionListener(PositionListener listener) {
	odometry.removeListener(listener);
    }

    /**
     * Add BatteryListener to the robot.
     * 
     * @param listener
     *            listener to be add.
     */
    public void addBatteryListener(BatteryListener listener) {
	powerManagement.addListener(listener);
    }

    /**
     * Remove BatteryListener from the robot.
     * 
     * @param listener
     *            listener to delete.
     */
    public void removeBatteryListener(BatteryListener listener) {
	powerManagement.removeListener(listener);
    }

    /**
     * @return true if the robot is connected.
     */
    public boolean isConnected() {
	return com.isConnected();
    }

    /**
     * Sets address of robot
     * 
     * @param hostname
     *            address to set
     */
    public void setAdddress(String hostname) {
	com.setAddress(hostname);
    }

    /**
     * Connect to the robot.
     * 
     * @param block
     *            blocking connection
     */
    public void connect(boolean block) {
	init();
	com.connectToServer(block);
    }

    /**
     * Disconnect from the current robot
     */
    public void disconnect() {
	com.disconnectFromServer();
    }

    /**
     * Set robot speed
     * 
     * @param vx
     *            Forward/Back speed
     * @param vy
     *            Left/Right speed
     * @param omega
     *            Turn speed
     */
    public void setVelocity(float vx, float vy, float omega) {
	if (!isBumper()) {
	    omniDrive.setVelocity(vx, vy, omega);
	}
    }

    /**
     * @return true if the bumper is bend.
     */
    public boolean isBumper() {
	return bumper.value();
    }

    /**
     * @return list of DistanceSensors.
     */
    public List<DistanceSensor> getDistanceSensors() {
	return distanceSensors;
    }

    /**
     * @return list of AnalogInputs.
     */
    public List<AnalogInput> getAnalogInputs() {
	return analogInputs;
    }

    /**
     * @return list of DigitalInputs.
     */
    public List<DigitalInput> getDigitalInputs() {
	return digitalInputs;
    }

    /**
     * @return list of DigitalOutputs.
     */
    public List<DigitalOutput> getDigitalOutputs() {
	return digitalOutputs;
    }

    /**
     * @return list of Relay
     */
    public List<Relay> getRelays() {
	return relays;
    }

    /**
     * @return Odometry
     */
    public Odometry getOdometry() {
	return odometry;
    }

    /**
     * @return Grappler
     */
    public Grappler getGrappler() {
	return grappler;
    }

    /**
     * Shutdown the robot
     */
    public void shutdown() {
	shutdown.shutdown();
    }

}