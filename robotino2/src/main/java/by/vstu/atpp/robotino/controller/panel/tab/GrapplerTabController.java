package by.vstu.atpp.robotino.controller.panel.tab;

import static javafx.collections.FXCollections.observableArrayList;
import static javafx.scene.input.KeyCode.ENTER;
import static javafx.scene.input.MouseButton.PRIMARY;

import java.util.Locale;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import by.vstu.atpp.robotino.controller.AbstractController;
import by.vstu.atpp.robotino.domain.GrapplerPosition;
import by.vstu.atpp.robotino.service.GrapplerService;
import by.vstu.atpp.robotino.util.validator.GrapplerValidator;
import by.vstu.atpp.robotino.util.validator.Validator;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.util.Pair;
import javafx.util.converter.NumberStringConverter;

/**
 * Controller that handles events of the GrapplerTab.fxml
 *
 * @author isap.vstu.by
 *
 */
@Component
public class GrapplerTabController extends AbstractController {

    @FXML
    private Label bottomLabel;
    @FXML
    private Label middleLabel;
    @FXML
    private Label topLabel;
    @FXML
    private Label gripperLabel;
    @FXML
    private TextField bottomField;
    @FXML
    private TextField middleField;
    @FXML
    private TextField topField;
    @FXML
    private TextField gripperField;
    @FXML
    private CheckBox bottomBox;
    @FXML
    private CheckBox middleBox;
    @FXML
    private CheckBox topBox;
    @FXML
    private CheckBox gripperBox;
    @FXML
    private ListView<String> posList;

    @Autowired
    private GrapplerService grapplerService;

    private Validator<GrapplerPosition> validator = new GrapplerValidator();
    private MenuItem editItem;
    private MenuItem deleteItem;
    private ObservableList<String> positionsItems;

    @FXML
    private void initialize() {
	bottomField.setTextFormatter(new TextFormatter<>(new NumberStringConverter(Locale.US)));
	middleField.setTextFormatter(new TextFormatter<>(new NumberStringConverter(Locale.US)));
	topField.setTextFormatter(new TextFormatter<>(new NumberStringConverter(Locale.US)));
	gripperField.setTextFormatter(new TextFormatter<>(new NumberStringConverter(Locale.US)));

	positionsItems = observableArrayList(grapplerService.getSavedPositionNames());

	posList.setItems(positionsItems);
	posList.setContextMenu(createListMenu());
	posList.setOnContextMenuRequested(event -> {
	    boolean enabled = posList.getSelectionModel().getSelectedIndex() != -1;
	    editItem.setDisable(!enabled);
	    deleteItem.setDisable(!enabled);
	});
	posList.setOnMouseClicked(event -> {
	    if (event.getButton() == PRIMARY && event.getClickCount() == 2) {
		grapplerService.setGrapplerPosition(
			grapplerService.getSavedPosition(posList.getSelectionModel().getSelectedItem()));
	    }
	});
	grapplerService.addStorePositionListener(position -> Platform.runLater(() -> createNewGialog(position)));
    }

    private ContextMenu createListMenu() {
	ContextMenu menu = new ContextMenu();
	MenuItem newItem = new MenuItem("New...");
	newItem.setOnAction(event -> createNewGialog(grapplerService.getCurrentGrapplerPosition()));
	editItem = new MenuItem("Edit...");
	editItem.setOnAction(event -> createEditDialog());
	deleteItem = new MenuItem("Delete");
	deleteItem.setOnAction(event -> createDeleteDialog());
	menu.getItems().addAll(newItem, editItem, deleteItem);
	return menu;
    }

    private void createNewGialog(GrapplerPosition initial) {
	Optional<Pair<String, GrapplerPosition>> result = new GripperDialog("New Position", initial).showAndWait();
	if (result.isPresent()) {
	    grapplerService.savePosition(result.get().getKey(), result.get().getValue());
	    updateSavedList();
	}
    }

    private void updateSavedList() {
	positionsItems.clear();
	positionsItems.addAll(grapplerService.getSavedPositionNames());
	posList.refresh();
    }

    private void createEditDialog() {
	String name = posList.getSelectionModel().getSelectedItem();
	Optional<Pair<String, GrapplerPosition>> result = new GripperDialog("Edit Position", name,
		grapplerService.getSavedPosition(name)).showAndWait();
	if (result.isPresent()) {
	    grapplerService.deleteSavedPosition(name);
	    grapplerService.savePosition(result.get().getKey(), result.get().getValue());
	    updateSavedList();
	}
    }

    private void createDeleteDialog() {
	Alert alert = new Alert(AlertType.CONFIRMATION);
	alert.setTitle("Delete");
	alert.setHeaderText("Do you really want to delete this?");

	Optional<ButtonType> result = alert.showAndWait();
	if (result.isPresent() && result.get() == ButtonType.OK) {
	    grapplerService.deleteSavedPosition(posList.getSelectionModel().getSelectedItem());
	    updateSavedList();
	}
    }

    @FXML
    private void toggleArm() {
	Alert alert = new Alert(AlertType.CONFIRMATION);
	alert.setTitle("Toggle Control");
	alert.setHeaderText("Do you really want to toggle mode between action and floating?\r\n"
		+ "Be careful, the grappler can damage itself when it falls or when it comes to an obstacle.");
	Optional<ButtonType> result = alert.showAndWait();
	if (result.isPresent() && result.get() == ButtonType.OK) {
	    grapplerService.toggleGrapplerControl();
	}
    }

    /**
     * Set grappler position that will be shown on the GUI
     *
     * @param grapplerPosition
     *            position that will be shown on the GUI
     */
    public void setGrapplerPosition(GrapplerPosition grapplerPosition) {
	Platform.runLater(() -> {
	    bottomLabel.setText(String.format("%3.3f", grapplerPosition.getBottomPosition()));
	    middleLabel.setText(String.format("%3.3f", grapplerPosition.getMiddlePosition()));
	    topLabel.setText(String.format("%3.3f", grapplerPosition.getTopPosition()));
	    gripperLabel.setText(String.format("%3.3f", grapplerPosition.getGrapplePosition()));
	});
    }

    @FXML
    private void comboPressed() {
	grapplerService.setPowerEnabled(!bottomBox.isSelected());
    }

    @FXML
    private void keyPressed(KeyEvent event) {
	if (event.getCode() == ENTER) {
	    GrapplerPosition position = new GrapplerPosition(getAngle(bottomField), getAngle(middleField),
		    getAngle(topField), getAngle(gripperField), 4.5f);
	    position = validator.validate(position);
	    bottomField.setText(String.valueOf(position.getBottomPosition()));
	    middleField.setText(String.valueOf(position.getMiddlePosition()));
	    topField.setText(String.valueOf(position.getTopPosition()));
	    gripperField.setText(String.valueOf(position.getGrapplePosition()));

	    grapplerService.setGrapplerPosition(position);
	}
    }

    private float getAngle(TextField field) {
	try {
	    return Float.parseFloat(field.getText());
	} catch (NumberFormatException e) {
	    return 0;
	}
    }

    private class GripperDialog extends Dialog<Pair<String, GrapplerPosition>> {

	private TextField nameField = new TextField();
	private TextField bottomField = new TextField();
	private TextField middleField = new TextField();
	private TextField topField = new TextField();
	private TextField gripperField = new TextField();

	public GripperDialog(String title, String name, GrapplerPosition position) {
	    this(title, position);
	    nameField.setText(name);
	}

	public GripperDialog(String title, GrapplerPosition position) {
	    this(title);
	    updateFields(position);
	}

	public GripperDialog(String title) {
	    setTitle(title);
	    setHeaderText("Enter grappler position");

	    getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);

	    GridPane grid = new GridPane();
	    grid.setHgap(10);
	    grid.setVgap(10);
	    grid.setPadding(new Insets(20, 150, 10, 10));

	    grid.add(new Label("Name:"), 0, 0);
	    grid.add(nameField, 1, 0);
	    grid.add(new Label("Bottom:"), 0, 1);
	    grid.add(bottomField, 1, 1);
	    grid.add(new Label("Middle:"), 0, 2);
	    grid.add(middleField, 1, 2);
	    grid.add(new Label("Top:"), 0, 3);
	    grid.add(topField, 1, 3);
	    grid.add(new Label("Gripper:"), 0, 4);
	    grid.add(gripperField, 1, 4);

	    getDialogPane().setContent(grid);

	    setResultConverter(dialogButton -> {
		if (dialogButton.getButtonData() == ButtonData.OK_DONE) {
		    GrapplerPosition position = new GrapplerPosition(getAngle(bottomField), getAngle(middleField),
			    getAngle(topField), getAngle(gripperField), 2f);
		    position = validator.validate(position);
		    updateFields(position);
		    return new Pair<>(nameField.getText(), position);
		}
		return null;
	    });
	}

	private void updateFields(GrapplerPosition position) {
	    bottomField.setText(String.valueOf(position.getBottomPosition()));
	    middleField.setText(String.valueOf(position.getMiddlePosition()));
	    topField.setText(String.valueOf(position.getTopPosition()));
	    gripperField.setText(String.valueOf(position.getGrapplePosition()));
	}

    }

}
