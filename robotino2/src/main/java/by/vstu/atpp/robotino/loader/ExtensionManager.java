package by.vstu.atpp.robotino.loader;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * This class loads Module and Action classes from folders.
 * 
 * @author isap.vstu.by
 *
 */
public class ExtensionManager {

    private static final Log LOG = LogFactory.getLog(ExtensionManager.class);

    private static final String ACTION_EXT = ".r3a";

    private static final String MODULE_EXT = ".r3m";

    private FileClassLoader cl = new FileClassLoader();

    private List<File> folders;

    private List<Class<?>> classes = new ArrayList<>();

    private List<File> moduleFiles = new ArrayList<>();

    private List<File> actionFiles = new ArrayList<>();

    /**
     * @param folders
     *            from this folders ExtensionManager loads Action and Module
     *            classes.
     */
    public ExtensionManager(List<File> folders) {
	this.folders = folders;
    }

    /**
     * Load all files from folders, defines them as new classes.
     * 
     * @return defined classes.
     */
    public Class<?>[] getClasses() {
	for (File folder : folders) {
	    Arrays.stream(folder.listFiles()).forEach(file -> {
		String name = file.getName();
		if (name.endsWith(MODULE_EXT)) {
		    moduleFiles.add(file);
		} else if (name.endsWith(ACTION_EXT)) {
		    actionFiles.add(file);
		}
	    });
	}
	moduleFiles.forEach(this::load);
	actionFiles.forEach(this::load);
	return classes.toArray(new Class<?>[0]);
    }

    private void load(File file) {
	try {
	    Class<?> clazz = cl.loadClass(file);
	    classes.add(clazz);
	} catch (ClassNotFoundException e) {
	    LOG.warn("can't load extension " + file.getName());
	}
    }
}
