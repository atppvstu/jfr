package by.vstu.atpp.robotino.util.validator;

import by.vstu.atpp.robotino.domain.Speed;

/**
 * This class is used to validate GrapplerPosition
 * 
 * @author isap.vstu.by
 *
 */
public class DefaultSpeedValidator implements Validator<Speed> {

    private static final float MAX_MOVE_SPEED = 0.5f;
    private static final float MAX_ROTATE_SPEED = 1.5f;

    /**
     * {@inheritDoc}
     */
    @Override
    public Speed validate(Speed speed) {
	float vx = check(speed.getVx(), MAX_MOVE_SPEED);
	float vy = check(speed.getVy(), MAX_MOVE_SPEED);
	float omega = check(speed.getOmega(), MAX_ROTATE_SPEED);
	return new Speed(vx, vy, omega);
    }

    private float check(float speed, float max) {
	return Math.signum(speed) * Math.min(Math.abs(speed), max);
    }

}
