package by.vstu.atpp.robotino.robot.listener;

import by.vstu.atpp.robotino.domain.GrapplerPosition;

public interface StorePositionListener {

    void onPositionStored(GrapplerPosition position);

}