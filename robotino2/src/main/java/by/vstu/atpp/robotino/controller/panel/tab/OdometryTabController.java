package by.vstu.atpp.robotino.controller.panel.tab;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import by.vstu.atpp.robotino.controller.AbstractController;
import by.vstu.atpp.robotino.domain.Position;
import by.vstu.atpp.robotino.domain.Speed;
import by.vstu.atpp.robotino.service.PriorityDriveService;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;

/**
 * Controller that handles events of the OdometryTab.fxml
 *
 * @author isap.vstu.by
 *
 */
@Component
public class OdometryTabController extends AbstractController {

    /**
     * Zero values of the speed and position that shown while robot is not connected
     */
    public static final double[] ZERO_VALUES = { 0, 0, 0 };

    @FXML
    private Label vxLabel;
    @FXML
    private Label vyLabel;
    @FXML
    private Label omegaLabel;
    @FXML
    private Label xLabel;
    @FXML
    private Label yLabel;
    @FXML
    private Label phiLabel;
    @FXML
    private Slider speedSlider;

    @Autowired
    private PriorityDriveService driveService;

    private float sliderSpeed = 0.1f;

    @FXML
    private void initialize() {
	speedSlider.valueProperty().addListener(arg -> {
	    sliderSpeed = (float) speedSlider.getValue();
	    Speed curSpeed = driveService.getTargetSpeed();
	    float vx, vy, omega;
	    if (curSpeed.getVx() != 0) {
		vx = sliderSpeed * Math.signum(curSpeed.getVx());
	    } else {
		vx = 0;
	    }
	    if (curSpeed.getVy() != 0) {
		vy = sliderSpeed * Math.signum(curSpeed.getVy());
	    } else {
		vy = 0;
	    }
	    if (curSpeed.getOmega() != 0) {
		omega = sliderSpeed * Math.signum(curSpeed.getOmega());
	    } else {
		omega = 0;
	    }
	    driveService.setManualSpeed(new Speed(vx, vy, omega));
	});
    }

    @FXML
    private void resetOdometry() {
	driveService.setOdometry(new Position());
    }

    /**
     * Show value of the speed on the OdometryTab
     *
     * @param speed
     *            speed value that will be shown
     */
    public void setSpeed(Speed speed) {
	Platform.runLater(() -> {
	    vxLabel.setText(String.format("%+.3f", speed.getVx()));
	    vyLabel.setText(String.format("%+.3f", speed.getVy()));
	    omegaLabel.setText(String.format("%+.3f", speed.getOmega()));
	});
    }

    /**
     * Show value of the robot position on the OdometryTab
     *
     * @param position
     *            position value that will be shown
     */
    public void setPosition(Position position) {
	Platform.runLater(() -> {
	    xLabel.setText(String.format("%+.3f", position.getX()));
	    yLabel.setText(String.format("%+.3f", position.getY()));
	    phiLabel.setText(String.format("%+.3f", position.getPhi()));
	});
    }

    /**
     * @return value of speed set by user on slider
     */
    public float getSliderSpeed() {
	return sliderSpeed;
    }

}
