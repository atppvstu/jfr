package by.vstu.atpp.robotino.robot;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import by.vstu.atpp.robotino.robot.listener.ConnectListener;
import rec.robotino.api2.Com;

/**
 * This class extends Com class from robotino api2
 * 
 * @author isap.vstu.by
 *
 */
public class DefaultCom extends Com {

    private static final Log LOG = LogFactory.getLog(DefaultCom.class);

    private List<ConnectListener> listeners = new ArrayList<>();

    /**
     * Default constructor
     */
    public DefaultCom() {
	ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
	service.scheduleAtFixedRate(this::processEvents, 0, 20, MILLISECONDS);
    }

    /**
     * Called when connection with robot was established
     */
    @Override
    public void connectedEvent() {
	LOG.debug("Connected");
	for (ConnectListener listener : listeners) {
	    listener.onConnected();
	}

    }

    /**
     * Called when connection with robot was broken with error
     */
    @Override
    public void errorEvent(String errorStr) {
	LOG.info("Disconnected with error: " + errorStr);
	for (ConnectListener listener : listeners) {
	    listener.onError(errorStr);
	}
    }

    /**
     * Called when connection with robot was stopped
     */
    @Override
    public void connectionClosedEvent() {
	LOG.debug("Disconnected");
	for (ConnectListener listener : listeners) {
	    listener.onDisconnected();
	}
    }

    /**
     * Add ConnectListener to the robot.
     * 
     * @param listener
     *            listener that has to be added.
     */
    public void addListener(ConnectListener listener) {
	listeners.add(listener);
    }

    /**
     * Remove ConnectListener from the robot.
     * 
     * @param listener
     *            listener that has to be deleted.
     */
    public void removeListener(ConnectListener listener) {
	listeners.remove(listener);
    }
}