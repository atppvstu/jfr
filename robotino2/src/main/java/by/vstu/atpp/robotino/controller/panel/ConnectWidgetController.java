package by.vstu.atpp.robotino.controller.panel;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import by.vstu.atpp.robotino.controller.AbstractController;
import by.vstu.atpp.robotino.service.ConnectService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;

/**
 * Controller that handles events of the StatusWidget.fxml
 * 
 * @author isap.vstu.by
 *
 */
public class ConnectWidgetController extends AbstractController {

    private static final String CONNECT = "Connect";
    private static final String DISCONNECT = "Disconnect";

    @FXML
    private ComboBox<String> address;
    @FXML
    private Button connectButton;

    @Autowired
    private ConnectService connectService;

    private boolean connected = false;
    private List<String> hosts;

    @FXML
    private void initialize() {
	ObservableList<String> items = FXCollections.observableArrayList(hosts);
	address.setItems(items);
	address.getSelectionModel().select(0);
    }

    @FXML
    private void connect() {
	String hostname = address.getSelectionModel().getSelectedItem();
	if (connected) {
	    connectService.disconnect();
	    connectButton.setText(CONNECT);
	    address.setDisable(false);
	} else {
	    connectService.connect(hostname, false);
	    connectButton.setText(DISCONNECT);
	    address.setDisable(true);
	}
	connected = !connected;
    }

    /**
     * Set list of the robot addresses into combobox
     * 
     * @param hosts
     *            hosts that will be setted
     */
    public void setHosts(List<String> hosts) {
	this.hosts = hosts;
    }

}
