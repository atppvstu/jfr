package by.vstu.atpp.robotino.domain;

/**
 * That class defines battery status of the robot
 * 
 * @author isap.vstu.by
 *
 */
public class BatteryStatus {

    /**
     * Status of battery that shown while robot is not connected
     */
    public static final BatteryStatus OFFLINE = new BatteryStatus(0.0f, false);

    private Float current;

    private boolean isCharging;

    /**
     * @param current
     *            Battery voltage
     * @param isCharging
     *            Battery charging status
     */
    public BatteryStatus(Float current, boolean isCharging) {
	this.current = current;
	this.isCharging = isCharging;
    }

    /**
     * 
     * @return Battery voltage
     */
    public Float getCurrent() {
	return current;
    }

    /**
     * 
     * @return Battery charging status
     */
    public boolean isCharging() {
	return isCharging;
    }

}
