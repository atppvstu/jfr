package by.vstu.atpp.robotino.service;

import java.util.List;

import by.vstu.atpp.robotino.domain.GrapplerPosition;
import by.vstu.atpp.robotino.robot.listener.StorePositionListener;
import rec.robotino.api2.GrapplerReadings;

/**
 * This class is used to control grappler of the robot
 *
 * @author isap.vstu.by
 *
 */
public interface GrapplerService extends Service {

    /**
     * @return raw grappler readings
     */
    GrapplerReadings getGrapplerReadings();

    /**
     * @return current position of grappler
     */
    GrapplerPosition getCurrentGrapplerPosition();

    /**
     * Set target grappler position
     *
     * @param position
     *            position to set
     */
    void setGrapplerPosition(GrapplerPosition position);

    /**
     * Enable/disable power of grappler
     *
     * @param value
     *            of power enable
     */
    void setPowerEnabled(boolean value);

    /**
     * Toggle between acting and floating mode
     */
    void toggleGrapplerControl();

    /**
     * Get saved position by name
     *
     * @param name
     *            of the position
     * @return saved position
     */
    GrapplerPosition getSavedPosition(String name);

    /**
     * Save position by name
     *
     * @param name
     *            of the position
     * @param position
     *            position to save
     */
    void savePosition(String name, GrapplerPosition position);

    /**
     * Delete saved position by name
     *
     * @param name
     *            name of the position
     */
    void deleteSavedPosition(String name);

    /**
     * Get all saved position names
     *
     * @return Unmodifiable List of the all position names
     */
    List<String> getSavedPositionNames();

    /**
     * Adds listener of StorePosition button (green button) on the grappler
     * 
     * @param listener
     *            listener to add
     */
    void addStorePositionListener(StorePositionListener listener);

}
