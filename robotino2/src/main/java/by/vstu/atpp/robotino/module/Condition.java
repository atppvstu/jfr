package by.vstu.atpp.robotino.module;

/**
 * 
 * This class is used to set stop condition for a Module. You can add a stop
 * condition to the Module using
 * {@link AbstractModule#setStopCondition(Condition) setStopCondition} method.
 * For example:
 * 
 * <pre>
 * <code>
 * public MyAction(MyModule module, DataInputService inputService) {
 *		module.setStopCondition(new Condition() {
 *		&#64;Override
 *		public boolean test() {
 *			return inputService.getSensors().get(0).distance() &lt; 0.1f;
 *		}
 *	});
 *	addLast(module);
 * }
 * </code>
 * </pre>
 * 
 * @author isap.vstu.by
 */
public interface Condition {

    /**
     * Checks that condition is satisfied.
     * 
     * @return true if Module should be breaked.
     */
    boolean test();

}
