package by.vstu.atpp.robotino.action.impl;

import org.springframework.beans.factory.annotation.Autowired;

import by.vstu.atpp.robotino.action.AbstractAction;
import by.vstu.atpp.robotino.action.ActionClass;
import by.vstu.atpp.robotino.module.impl.GoToColoredObjectModule;
import by.vstu.atpp.robotino.module.impl.AlarmModule;
import by.vstu.atpp.robotino.service.SensorService;
import by.vstu.atpp.robotino.service.GuiService;

/**
 * This action consists of the following modules:
 * <ul>
 * <li>{@link GoToColoredObjectModule GoToColoredObjectModule} with stop
 * condition by distance</li>
 * </ul>
 * 
 * @author isap.vstu.by
 *
 */
@ActionClass
public class GoToColoredObjectActon extends AbstractAction {

    @Autowired
    private SensorService dataInputService;

    /**
     * @param userInputService
     *            service to set color in the goToColoredObjectModule.
     * @param goToColoredObjectModule
     *            The module to perform.
     * @param viuModule
     *            The module to perform.
     * 
     */
    @Autowired
    public GoToColoredObjectActon(GuiService userInputService, GoToColoredObjectModule goToColoredObjectModule,
	    AlarmModule viuModule) {
	goToColoredObjectModule.setStopCondition(() -> {
	    return dataInputService.getSensors().get(0).distance() < 0.1f;
	});
	goToColoredObjectModule.setColors(userInputService.getRangeColor());
	addLast(goToColoredObjectModule);
	addLast(viuModule);
    }

}
