package by.vstu.atpp.robotino.robot;

import java.util.ArrayList;
import java.util.List;

import by.vstu.atpp.robotino.domain.GrapplerPosition;
import by.vstu.atpp.robotino.robot.listener.StorePositionListener;
import rec.robotino.api2.Grappler;
import rec.robotino.api2.GrapplerReadings;

/**
 * This class extends Grappler class from robotino api2
 * 
 * @author isap.vstu.by
 *
 */
public class DefaultGrappler extends Grappler {

    private List<StorePositionListener> listeners = new ArrayList<>();

    /**
     * Called when readings from grappler has been received
     */
    @Override
    public void storePositionsEvent(GrapplerReadings readings) {
	GrapplerPosition position = new GrapplerPosition(readings);
	for (StorePositionListener listener : listeners) {
	    listener.onPositionStored(position);
	}

    }

    /**
     * Add listener of grappler position
     * 
     * @param listener
     *            listener to add
     */
    public void addListener(StorePositionListener listener) {
	listeners.add(listener);
    }

    /**
     * Remove listener of grappler position
     * 
     * @param listener
     *            listener to remove
     */
    public void removeListener(StorePositionListener listener) {
	listeners.remove(listener);
    }

}
