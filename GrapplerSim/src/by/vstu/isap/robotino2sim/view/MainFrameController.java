package by.vstu.isap.robotino2sim.view;

import java.util.Timer;
import java.util.TimerTask;

import by.vstu.isap.robotino2sim.domain.GrapplerPosition;
import by.vstu.isap.robotino2sim.driver.GrapplerDriver;
import by.vstu.isap.robotino2sim.service.RobotActuatorService;
import by.vstu.isap.robotino2sim.validator.GrapplerValidator;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;

public class MainFrameController {

	@FXML
	private Pane pane;

	private int lengths[] = { 200, 100, 80, 20 };
	private Line lines[];

	private GrapplerDriver driver = new ManualGrapplerDriver();
	private GrapplerPosition currentPosition = new GrapplerPosition(0, 0, 0, 0);
	private GrapplerValidator validator = new GrapplerValidator();

	@FXML
	private TextField topField;

	@FXML
	private TextField midField;

	@FXML
	private TextField botField;

	@FXML
	private TextField grapField;

	@FXML
	public void initialize() {
		lines = new Line[4];
		for (int i = 0; i < lines.length; i++) {
			Line line = new Line();
			line.setStrokeWidth(3);
			lines[i] = line;
			pane.getChildren().add(line);
		}

		driver.setActuatorService(new GraphicalRobotActuatorService());
		Timer timer = new Timer(true);
		timer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				driver.execute(currentPosition);
			}
		}, 0, 100);
	}

	private class GraphicalRobotActuatorService implements RobotActuatorService {

		private double xPos[] = { 200, 0, 0, 0, 0 };
		private double yPos[] = { 400, 0, 0, 0, 0 };

		@Override
		public void setGrrapplerPosition(GrapplerPosition position) {

			position = validator.validate(offsetPosition(position));

			double aBot = position.getBottomPosition() + 5;
			double xBot = lengths[0] * sin(aBot);
			double yBot = lengths[0] * cos(aBot);

			xPos[1] = xPos[0] + xBot;
			yPos[1] = yPos[0] + yBot;

			double aMid = position.getBottomPosition() - position.getMiddlePosition() + 130;
			double xMid = lengths[1] * sin(aMid);
			double yMid = lengths[1] * cos(aMid);

			xPos[2] = xPos[1] + xMid;
			yPos[2] = yPos[1] + yMid;

			double aTop = aMid + position.getTopPosition() - 125;
			double xTop = lengths[2] * sin(aTop);
			double yTop = lengths[2] * cos(aTop);

			xPos[3] = xPos[2] + xTop;
			yPos[3] = yPos[2] + yTop;

			double xTopGrap = lengths[3] * sin(aTop);
			double yTopGrap = lengths[3] * cos(aTop);
			xPos[4] = xPos[3] + xTopGrap;
			yPos[4] = yPos[3] + yTopGrap;

			for (int i = 0; i < lines.length; i++) {
				lines[i].setStartX(xPos[i]);
				lines[i].setStartY(yPos[i]);
				lines[i].setEndX(xPos[i + 1]);
				lines[i].setEndY(yPos[i + 1]);
			}

			int red = (int) position.getGrapplePosition();
			if (red > 255) {
				red = 255;
			}

			double colorRange = GrapplerValidator.BORDER_ANGLES[3][1] - GrapplerValidator.BORDER_ANGLES[3][0];
			int color = (int) (255 * (position.getGrapplePosition() - GrapplerValidator.BORDER_ANGLES[3][0])
					/ colorRange);
			lines[3].setStroke(Color.rgb(color, 255 - color, 0));

			currentPosition = position;
		}

		private double sin(double degrees) {
			return Math.sin(Math.toRadians(degrees));
		}

		private double cos(double degrees) {
			return Math.cos(Math.toRadians(degrees));
		}

		private GrapplerPosition offsetPosition(GrapplerPosition position) {
			float botPos = currentPosition.getBottomPosition();
			float botOffset = position.getBottomSpeed();
			if (position.getBottomPosition() > botPos + botOffset) {
				botPos += botOffset;
			} else if (position.getBottomPosition() < botPos - botOffset) {
				botPos -= botOffset;
			} else {
				botPos = position.getBottomPosition();
			}
			float midPos = currentPosition.getMiddlePosition();
			float midOffset = position.getMiddleSpeed();
			if (position.getMiddlePosition() > midPos + midOffset) {
				midPos += midOffset;
			} else if (position.getMiddlePosition() < midPos - midOffset) {
				midPos -= midOffset;
			} else {
				midPos = position.getMiddlePosition();
			}
			float topPos = currentPosition.getTopPosition();
			float topOffset = position.getTopSpeed();
			if (position.getTopPosition() > topPos + topOffset) {
				topPos += topOffset;
			} else if (position.getTopPosition() < topPos - topOffset) {
				topPos -= topOffset;
			} else {
				topPos = position.getTopPosition();
			}
			float grapPos = currentPosition.getGrapplePosition();
			float grapOffet = position.getGrappleSpeed();
			if (position.getGrapplePosition() > grapPos + grapOffet) {
				grapPos += grapOffet;
			} else if (position.getGrapplePosition() < grapPos - grapOffet) {
				grapPos -= grapOffet;
			} else {
				grapPos = position.getGrapplePosition();
			}
			return new GrapplerPosition(botPos, midPos, topPos, grapPos, position.getBottomSpeed(),
					position.getMiddleSpeed(), position.getTopSpeed(), position.getGrappleSpeed());
		}
	}

	private class ManualGrapplerDriver extends GrapplerDriver {

		@Override
		public void execute(GrapplerPosition position) {
			double botPos = parse(position.getBottomPosition(), botField.getText());
			double midPos = parse(position.getMiddlePosition(), midField.getText());
			double topPos = parse(position.getTopPosition(), topField.getText());
			double grapPos = parse(position.getGrapplePosition(), grapField.getText());
			GrapplerPosition position2 = new GrapplerPosition((float) botPos, (float) midPos, (float) topPos,
					(float) grapPos);
			actuatorService.setGrrapplerPosition(position2);
			System.out.println(currentPosition);
		}

		private double parse(double oldValue, String newValue) {
			try {
				return Double.parseDouble(newValue);
			} catch (Exception e) {
				return oldValue;
			}
		}

	}

}
