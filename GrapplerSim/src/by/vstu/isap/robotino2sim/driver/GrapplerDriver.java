package by.vstu.isap.robotino2sim.driver;

import by.vstu.isap.robotino2sim.domain.GrapplerPosition;
import by.vstu.isap.robotino2sim.service.RobotActuatorService;

public abstract class GrapplerDriver {

	protected RobotActuatorService actuatorService;

	abstract public void execute(GrapplerPosition position);

	public void setActuatorService(RobotActuatorService actuatorService) {
		this.actuatorService = actuatorService;
	}

}
