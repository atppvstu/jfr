package by.vstu.isap.robotino2sim.validator;

import by.vstu.isap.robotino2sim.domain.GrapplerPosition;

public class GrapplerValidator implements Validator<GrapplerPosition> {

	public static float[][] BORDER_ANGLES = { { 72.140762F, 225.513184F }, { 21.700880F, 228.445740F },
			{ 30.498533F, 216.129028F }, { 158.651016F, 173.607040F } };
	public static float MAX_SPEED = 10f;

	@Override
	public GrapplerPosition validate(GrapplerPosition position) {
		float botPos = validate(position.getBottomPosition(), BORDER_ANGLES[0][0], BORDER_ANGLES[0][1]);
		float midPos = validate(position.getMiddlePosition(), BORDER_ANGLES[1][0], BORDER_ANGLES[1][1]);
		float topPos = validate(position.getTopPosition(), BORDER_ANGLES[2][0], BORDER_ANGLES[2][1]);
		float grapPos = validate(position.getGrapplePosition(), BORDER_ANGLES[3][0], BORDER_ANGLES[3][1]);

		float botSpeed = validate(position.getBottomSpeed(), 0f, MAX_SPEED);
		float midSpeed = validate(position.getMiddleSpeed(), 0f, MAX_SPEED);
		float topSpeed = validate(position.getTopSpeed(), 0f, MAX_SPEED);
		float grapSpeed = validate(position.getGrappleSpeed(), 0f, MAX_SPEED);

		return new GrapplerPosition(botPos, midPos, topPos, grapPos, botSpeed, midSpeed, topSpeed, grapSpeed);
	}

	private float validate(float cur, float min, float max) {
		if (cur < min) {
			return min;
		}
		if (cur > max) {
			return max;
		}
		return cur;
	}

}
