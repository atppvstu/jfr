package by.vstu.isap.robotino2sim.validator;

public interface Validator<T> {

	T validate(T entity);

}
