package by.vstu.isap.robotino2sim.domain;

public class GrapplerPosition {

	private float bottomPosition;
	private float middlePosition;
	private float topPosition;
	private float grapplePosition;

	private float bottomSpeed;
	private float middleSpeed;
	private float topSpeed;
	private float grappleSpeed;

	public GrapplerPosition(float bottomPosition, float middlePosition, float topPosition, float grapplePosition) {
		this.bottomPosition = bottomPosition;
		this.middlePosition = middlePosition;
		this.topPosition = topPosition;
		this.grapplePosition = grapplePosition;
		bottomSpeed = 1f;
		middleSpeed = 1f;
		topSpeed = 1f;
		grappleSpeed = 1f;
	}

	public GrapplerPosition(float bottomPosition, float middlePosition, float topPosition, float grapplePosition,
			float speed) {
		this.bottomPosition = bottomPosition;
		this.middlePosition = middlePosition;
		this.topPosition = topPosition;
		this.grapplePosition = grapplePosition;
		bottomSpeed = speed;
		middleSpeed = speed;
		topSpeed = speed;
		grappleSpeed = speed;
	}

	public GrapplerPosition(float bottomPosition, float middlePosition, float topPosition, float grapplePosition,
			float bottomSpeed, float middleSpeed, float topSpeed, float grappleSpeed) {
		this.bottomPosition = bottomPosition;
		this.middlePosition = middlePosition;
		this.topPosition = topPosition;
		this.grapplePosition = grapplePosition;
		this.bottomSpeed = bottomSpeed;
		this.middleSpeed = middleSpeed;
		this.topSpeed = topSpeed;
		this.grappleSpeed = grappleSpeed;
	}

	public float getBottomPosition() {
		return bottomPosition;
	}

	public float getMiddlePosition() {
		return middlePosition;
	}

	public float getTopPosition() {
		return topPosition;
	}

	public float getGrapplePosition() {
		return grapplePosition;
	}

	public float getBottomSpeed() {
		return bottomSpeed;
	}

	public float getMiddleSpeed() {
		return middleSpeed;
	}

	public float getTopSpeed() {
		return topSpeed;
	}

	public float getGrappleSpeed() {
		return grappleSpeed;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("GrapplerPosition [");
		builder.append(bottomPosition);
		builder.append(", ");
		builder.append(middlePosition);
		builder.append(", ");
		builder.append(topPosition);
		builder.append(", ");
		builder.append(grapplePosition);
		builder.append(", ");
		builder.append(bottomSpeed);
		builder.append(", ");
		builder.append(middleSpeed);
		builder.append(", ");
		builder.append(topSpeed);
		builder.append(", ");
		builder.append(grappleSpeed);
		builder.append("]");
		return builder.toString();
	}

}
